<?php
ob_start();
session_start();
error_reporting(E_ALL & ~E_WARNING);
// error_reporting(0);

// ini settings
ini_set("memory_limit", "1024M");
ini_set('post_max_size', '20M');
ini_set('max_execution_time', 600);
ini_set('session.gc_maxlifetime', 24*60*40);
session_set_cookie_params(3600);

// Sever constants
define('SERVER', $_SERVER['SERVER_NAME']);
define ('PAGE', pathinfo(basename($_SERVER['PHP_SELF']), PATHINFO_FILENAME));
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('SCHEME', $_SERVER['REQUEST_SCHEME']);
define('PORT', $_SERVER['SERVER_PORT']);
define('REQUEST_URI', $_SERVER['REQUEST_URI']);
define('SCRIPT_NAME', $_SERVER['SCRIPT_NAME']);
define('REFERER', !in_array('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER']:'');

// SQL database parameters
if (SERVER != 'localhost' AND SERVER != '127.0.0.1' ) {
    define('BASE_PATH', '/2021vtu/', true);
    define('DB_NAME', 'generals_2021vtu');
    define('DB_USER', 'generals_2021vtu_super');
    define('DB_PASSWORD', 'g~(dU-Qb$iBp');
    define('DB_HOST', 'localhost');
}else{
    define('BASE_PATH', '/vtu-resell/', true);
    define('DB_NAME', '2021vtu');
    define('DB_USER', 'root');
    define('DB_PASSWORD', '');
    define('DB_HOST', 'localhost');
}

// Application constants
define('BASE_URL', SCHEME.'://'.SERVER.BASE_PATH);
define('MODEL_DIR', ROOT.BASE_PATH.'models/');
define('CONTROLLER_DIR', ROOT.BASE_PATH.'controllers/');
define('INCLUDES_DIR', ROOT.BASE_PATH.'includes/');
define('VENDOR_DIR', ROOT.BASE_PATH.'vendor/');
define('CLASS_DIR', ROOT.BASE_PATH.'classes/');
define('COMPONENT_DIR', ROOT.BASE_PATH.'components/');

define('USER_ROOT', '');
define('ADMIN_ROOT', 'web/');
define('UPLOADS_DIR', 'uploads/');
define('UPLOADS_PATH',  ROOT.BASE_PATH.UPLOADS_DIR);
define('PRODUCTS_UPLOADS_DIR', 'products/');
define('PROFILE_UPLOADS_DIR', 'profiles/');
define('LOGO_UPLOADS_DIR', 'logos/');

// API constants
define('APIURL', 'https://vtutopupbox.com/b2bhub/api/');
define('APIUSER', '09036830349');
define('APIPASS', 'D3vfr3$#3r');

// Page name constant
$page = explode('.', PAGE);
define('PAGE_NAME', $page['0']);

// Requirements
require_once(VENDOR_DIR.'autoload.php');
require_once('Database.php');
require_once("mysql.session.php");
// require_once INCLUDES_DIR.'transactionConnect.php';

// Database Coonection
$dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST . "";
$pdo = "";
try {
    $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
$db = new Database($pdo);

// Initialize Models
require_once MODEL_DIR.'Utility.php';
$utility = new Utility($db);

require_once MODEL_DIR.'App.php';
$app = new APP($db);
$appInfo = $app->getAppInfo();

require_once MODEL_DIR.'User.php';
$user = new User($db);

include_once INCLUDES_DIR.'language.php';
include_once INCLUDES_DIR.'session_check.php';
