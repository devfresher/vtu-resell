<?php
require_once './components/head.php';
include_once MODEL_DIR.'Beneficiary.php';

$wallet = new Wallet($db);
$beneficiary = new Beneficiary($db);

$myBeneficiary = $beneficiary->getUserBeneficiaries($user->currentUser->id);

?>
		<!--begin::Page Vendors Styles(used by this page)-->
		<link href="<?php echo BASE_URL.USER_ROOT?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" style="background-image: url(assets/media/bg/bg-10.jpg)" class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">
		<!--begin::Main-->
		
		<?php include_once COMPONENT_DIR.'mobileHeader.php'; ?>

		<div class="d-flex flex-column flex-root">

			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">

				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

					<?php include_once COMPONENT_DIR.'toolbar.php';?>

					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

						<?php include_once COMPONENT_DIR.'subToolBar.php'?>

						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">

							<!--begin::Container-->
							<div class="container">

								<!--begin::Wallet-->
								<!--begin::Row-->
								<div class="row">
									<div class="col-xl-3">
										<?php include_once COMPONENT_DIR.'walletBallance.php'?>
									</div>
									<div class="col-xl-9">
                                        <div class="card card-custom card-stretch">
                                            <div class="card-header">
                                                <div class="card-toolbar">
                                                    <h3><b><i class="fa fa-address-book"></i> My Beneficiaries</b></h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                                                    <thead>
                                                        <tr>
                                                            <th title="Field #1" class="custom-th">S/No</th>
                                                            <th title="Field #2" class="custom-th">Receipient</th>
                                                            <th title="Field #5" class="custom-th">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    if($myBeneficiary != NULL) { $i = 1;
                                                        foreach($myBeneficiary as $myBeneficiaries) { ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $myBeneficiaries["phone_number"]; ?></td>
                                                                <td>
                                                                    <a href="" class="btn btn-primary">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    
                                                                    <button class="deleteBeneficiary" beneficiary="<?php echo $myBeneficiaries["phone_number"]; ?>">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        <?php $i++; }
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
								</div>
								<!--end::Wallet-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!--end::Content-->
					
					<?php include_once COMPONENT_DIR.'footer.php';?>

				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->

		
		<?php include_once COMPONENT_DIR.'quickUser.php';?>

		<?php include_once COMPONENT_DIR.'scrollTop.php';?>
		<?php include_once COMPONENT_DIR.'js.php';?>
		
		<!--begin::Page Scripts(used by this page)-->
		<script src="<?php echo BASE_URL?>assets/js/pages/crud/ktdatatable/base/txn-table.js"></script>
		<script src="<?php echo BASE_URL?>assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
		<script src="<?php echo BASE_URL?>assets/js/pages/switch.js"></script>
		<script src="<?php echo BASE_URL?>assets/js/pages/select2.js"></script>
		<?php include_once COMPONENT_DIR.'message.php'?>

		<script>
			// $(document).ready(function() {
				console.log($('.deleteBeneficiary'));
				$(document).on("click", ".deleteBeneficiary", function() {
					var button = $(this);
					var beneficiary = button.attr("beneficiary");
					
					console.log(beneficiary);
					alert(1111);
				});
			// })

			
		</script>
		<!--end::Page Scripts-->
	</body>
	<!--end::Body-->
</html>