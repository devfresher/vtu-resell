<?php
require_once MODEL_DIR.'Http.php';

use HttpRequest;

class Monnify
{
    /**
     * base url
     *
     * @var
     */
    private $baseUrl;
    private $v1 = "/api/v1/";
    private $v2 = "/api/v2/";


    private $oAuth2Token = '';
    private $oAuth2TokenExpires = '';

    public function __construct($config)
    {
        $this->baseUrl = $config['baseUrl'];
        $this->config = $config;
    }


    private function getBasicAuthHeader()
    {
        $authHeader = "Authorization: Basic ".base64_encode($this->config['apiKey'].':'.$this->config['secKey']);
        return $authHeader;
    }

    private function getOauth2Header()
    {
        if (time() >= $this->oAuth2TokenExpires) {
            $this->getToken();
            $authHeader = "Authorization: Bearer ".$this->oAuth2Token;
        }

        return $authHeader;
    }


    /**
     * @return $this
     */
    private function getToken()
    {
        $endpoint = "{$this->baseUrl}{$this->v1}auth/login";

        $response = json_decode(
            HttpRequest::post(
                $endpoint,
                [$this->getBasicAuthHeader()]
            )
        );

        if ($response->requestSuccessful) {
            $this->oAuth2Token = $response->responseBody->accessToken;
            $this->oAuth2TokenExpires = ((time() + $response->responseBody->expiresIn) - 60);
        }

        return $this;
    }


    /**
     * This enables you to retrieve all banks supported by Monnify for collections and disbursements.
     * @return array
     */
    public function getBanks()
    {
        $endpoint = "{$this->baseUrl}{$this->v1}banks";

        $response = json_decode(
            HttpRequest::post(
                $endpoint,
                [$this->getOauth2Header()]
            )
        );

        if ($response->requestSuccessful){
            return $response->responseBody;
        }
    }

    public function reserveAccount(string $accountReference, string $accountName, string $customerEmail, string $customerName = null, string $customerBvn = null, string $currencyCode = null, bool $restrictPaymentSource = false, $allowedPaymentSources = [])
    {
        if ($restrictPaymentSource && is_null($allowedPaymentSources))
            throw ("Allowed Payment Sources can't be null if payment source is restricted");

        $endpoint = "{$this->baseUrl}{$this->v2}bank-transfer/reserved-accounts";

        $requestBody = [
            "accountReference" => $accountReference,
            "accountName" => $accountName,
            "currencyCode" => $currencyCode ?? $this->config['defaultCurrencyCode'],
            "contractCode" => $this->config['contractCode'],
            "customerEmail" => $customerEmail,
            // "preferredBanks" => ['035', '232', '50515'],
            "getAllAvailableBanks"=> true,
            // "restrictPaymentSource" => $restrictPaymentSource,
        ];

        if ((!is_null($customerName)) && (!empty(trim($customerName))))
            $requestBody['customerName'] = $customerName;

        if ((!is_null($customerBvn)) && (!empty(trim($customerBvn))))
            $requestBody['bvn'] = $customerBvn;

        // if (!is_null($allowedPaymentSources))
        //     $requestBody['allowedPaymentSources'] = $allowedPaymentSources;

        $response = json_decode(
            HttpRequest::post(
                $endpoint, 
                [$this->getOauth2Header()], 
                $requestBody
            )
        );

        if ($response->requestSuccessful) {
            return $response->responseBody;
        }
    }


    public function getReservedAccountDetails(string $accountReference)
    {
        $endpoint = "{$this->baseUrl}{$this->v1}bank-transfer/reserved-accounts/$accountReference";

        $response = json_decode(
            HttpRequest::get(
                $endpoint, 
                [$this->getOauth2Header()]
            )
        );

        if ($response->requestSuccessful){
            return $response->responseBody;
        }
    }


    public function updateReservedAccountSplitConfig(string $accountReference, $incomeSplitConfig = [])
    {
        $endpoint = "{$this->baseUrl}{$this->v1}bank-transfer/reserved-accounts/update-income-split-config/$accountReference";

        $response = json_decode(
            HttpRequest::put(
                $endpoint,
                [$this->getOauth2Header()],
                $incomeSplitConfig,
            )
        );

        if (!$response->requestSuccessful) {
            return $response->responseBody;
        }
    }


    public function getAllTransactionsForReservedAccount(string $accountReference, int $page = 0, int $size = 10)
    {
        $endpoint = "{$this->baseUrl}{$this->v1}bank-transfer/reserved-accounts/transactions?accountReference=$accountReference&page=$page&size=$size";

        $response = json_decode(
            HttpRequest::get(
                $endpoint, 
                [$this->getOauth2Header()]
            )
        );

        if (!$response->requestSuccessful){
            return $response->responseBody;
        }
    }


    public function deallocateReservedAccount(string $accountNumber)
    {
        $endpoint = "{$this->baseUrl}{$this->v1}bank-transfer/reserved-accounts/$accountNumber";

        $response = json_decode(
            HttpRequest::delete(
                $endpoint,
                [$this->getOauth2Header()]
            )
        );

        if (!$response->requestSuccessful){
            return $response->responseBody;
        }
    }



    /**
     * Allows you initialize a transaction on Monnify and returns a checkout URL which you can load within a browser to display the payment form to your customer.
     *
     * @param float $amount The amount to be paid by the customer
     * @param string $customerName Full name of the customer
     * @param string $customerEmail Email address of the customer
     * @param string $paymentReference Merchant's Unique reference for the transaction.
     * @param string $paymentDescription Description for the transaction. Will be returned as part of the account name on name enquiry for transfer payments.
     * @param string $redirectUrl A URL which user will be redirected to, on completion of the payment.
     * @param MonnifyPaymentMethods $monnifyPaymentMethods
     * @param MonnifyIncomeSplitConfig $incomeSplitConfig
     * @param string|null $currencyCode
     * @return array
     *
     * @throws MonnifyFailedRequestException
     * @link https://docs.teamapt.com/display/MON/Initialize+Transaction
     */
    public function initializeTransaction(float $amount, string $customerName, string $customerEmail, string $paymentReference, string $paymentDescription, string $redirectUrl, $monnifyPaymentMethods = [], $incomeSplitConfig = [], string $currencyCode = null)
    {
        $endpoint = "{$this->baseUrl}{$this->v1}merchant/transactions/init-transaction";

        $formData = [
            "amount" => $amount,
            "customerName" => trim($customerName),
            "customerEmail" => $customerEmail,
            "paymentReference" => $paymentReference,
            "paymentDescription" => trim($paymentDescription),
            "currencyCode" => $currencyCode ?? $this->config['default_currency_code'],
            "contractCode" => $this->config['contract_code'],
            "redirectUrl" => trim($redirectUrl),
            "paymentMethods" => $monnifyPaymentMethods,
        ];

        if ($incomeSplitConfig !== null)
            $formData["incomeSplitConfig"] = $incomeSplitConfig;

        $response = json_decode(
            HttpRequest::post(
                $endpoint,
                [$this->getOauth2Header()],
                $formData
            )
        );

        if (!$response->requestSuccessful){
            return $response->responseBody;
        }
    }


    /**
     * When Monnify sends transaction notifications, we add a transaction hash for security reasons. We expect you to try to recreate the transaction hash and only honor the notification if it matches.
     *
     * To calculate the hash value, concatenate the following parameters in the request body and generate a hash using the SHA512 algorithm:
     *
     * @param string $paymentReference Unique reference generated by the merchant for each transaction. However, will be the same as transactionReference for reserved accounts.
     * @param mixed $amountPaid The amount that was paid by the customer
     * @param string $paidOn Date and Time when payment happened in the format dd/mm/yyyy hh:mm:ss
     * @param string $transactionReference Unique transaction reference generated by Monnify for each transaction
     * @return string Hash of successful transaction
     *
     * @link https://docs.teamapt.com/display/MON/Calculating+the+Transaction+Hash
     */
    public function calculateTransactionHash(string $paymentReference, $amountPaid, string $paidOn, string $transactionReference)
    {
        $clientSK = $this->config['seckey'];
        return hash('sha512', "$clientSK|$paymentReference|$amountPaid|$paidOn|$transactionReference");
    }

    public function calculateTransactionHashFix(string $paymentReference, $amountPaid, string $paidOn, string $transactionReference)
    {
        $clientSK = $this->config['seckey'];
        return hash('sha512', "$clientSK|$paymentReference|$amountPaid|$paidOn|$transactionReference");
    }


    /**
     * We highly recommend that when you receive a notification from us, even after checking to ensure the hash values match,
     * you should initiate a get transaction status request to us with the transactionReference to confirm the actual status of that transaction before updating the records on your database.
     *
     * @param string $transactions Unique transaction reference generated by Monnify for each transaction
     * @return object
     *
     * @throws MonnifyFailedRequestException
     * @link https://docs.teamapt.com/display/MON/Get+Transaction+Status
     */
    public function getTransactionStatus(string $transactions)
    {
        $endpoint = "{$this->baseUrl}{$this->v2}transactions/$transactions/";

        $response = json_decode(
            HttpRequest::get(
                $endpoint,
                [$this->getOauth2Header()]
            )
        );
        
        if ($response->requestSuccessful){
            return $response->responseBody;
        }
    }


    /**
     * Allows you get virtual account details for a transaction using the transactionReference of an initialized transaction.
     * This is useful if you want to control the payment interface.
     * There are a lot of UX considerations to keep in mind if you choose to do this so we recommend you read this @link https://docs.teamapt.com/display/MON/Optimizing+Your+User+Experience.
     *
     * @param string $transactionReference
     * @param string $bankCode
     * @return array
     *
     * @link https://docs.teamapt.com/display/MON/Pay+with+Bank+Transfer
     */
    public function payWithBankTransfer(string $transactionReference, string $bankCode)
    {
        $endpoint = "{$this->baseUrl}{$this->v1}merchant/bank-transfer/init-payment";

        $response = json_decode(
            HttpRequest::post(
                $endpoint,
                [$this->getBasicAuthHeader()],
                [
                    "transactionReference" => $transactionReference,
                    "bankCode" => trim($bankCode),
                ], 
            )
        );

        if (!$response->requestSuccessful){
            return $response->responseBody;
        }
    }


    /**
     * To initiate a single transfer,  you will need to send a request to the endpoint below:
     *
     * If the merchant does not have Two Factor Authentication (2FA) enabled, the transaction will be processed instantly and the response will be as follows:
     *
     * If the merchant has Two Factor Authentication (2FA) enabled, a One Time Password (OTP) will be sent to the designated email address(es). That OTP will need to be supplied via the VALIDATE OTP REQUEST before the transaction can be approved. If 2FA is enabled,
     *
     * @param float $amount
     * @param string $reference
     * @param string $narration
     * @param MonnifyBankAccount $bankAccount
     * @param string|null $currencyCode
     * @return array
     *
     * @see https://docs.teamapt.com/display/MON/Initiate+Transfer
     */
    public function initiateTransferSingle(float $amount, string $reference, string $narration, string $bankAccountNumber, string $bankCode, string $currencyCode = null)
    {
        $endpoint = "{$this->baseUrl}{$this->v1}disbursements/single";

        $response = json_decode(
            HttpRequest::post(
                $endpoint,
                [$this->getBasicAuthHeader()],
                [
                    "amount" => $amount,
                    "reference" => trim($reference),
                    "narration" => trim($narration),
                    "bankCode" => $bankCode,
                    "accountNumber" => $bankAccountNumber,
                    "currency" => $currencyCode ?? $this->config['default_currency_code'],
                    "walletId" => $this->config['wallet_id']
                ]
            )
        );

        if (!$response->requestSuccessful){
            return $response->initiateTransferSingle;
        }
    }

}
