<?php
require_once MODEL_DIR.'Utility.php';

class Authorization extends Utility {
    public $currentUser;
    protected $responseBody;

    function __construct($db) {
        $this->db = $db;
        $this->table = new stdClass();
        $this->table->authorization = 'authorization';
        $this->table->pages = 'pages';
    }

    public function getPage($page) {
        $result = $this->db->getSingleRecord($this->table->pages, "*", "AND (id = '$page' OR slug = '$page')");

        if ($result != NULL) {
            $this->responseBody = $this->arrayToObject($result);
        }else {
            $this->responseBody = false;
        }
        
        return $this->responseBody;
    }

    public function getAllPages($pageType = '') {

        if ($pageType == '') {
            $result = $this->db->getAllRecords($this->table->pages, "*");
        } else {
            $result = $this->db->getAllRecords($this->table->pages, "*", "AND (type = '$pageType')");
        }

        if (count($result) > 0) {
            $this->responseBody = $this->arrayToObject($result);
        }else {
            $this->responseBody = false;
        }
        
        return $this->responseBody;
    }

    public function hasPagePermission($userId, $page)
    {
        $result = $this->db->getSingleRecord($this->table->pages, "*", "AND (slug = '$page' OR id = '$page')");

        if ($result != NULL) {
            $page = $result;
            $pageId = $page['id'];
    
            $result = $this->db->getSingleRecord($this->table->authorization, "*", "AND user_id = $userId AND page_id = $pageId");
    
            if ($result != NULL) {
                $this->responseBody = true;
            }else {
                $this->responseBody = false;
            }
        } else {
            $this->responseBody = false;
        }
        return $this->responseBody;
    }

    public function isAuthorizedAdmin()
    {
        if (isset($_SESSION['switched']) AND $_SESSION['switchd'] === true) {
            $this->responseBody = true;
        } else {
            $this->responseBody = false;
        }
        return $this->responseBody;
    }

    public function grantPermission($userId, $pageId)
    {
        $thePage = $this->getPage($pageId);
        $permitted = $this->hasPagePermission($userId, $pageId);

        if ($thePage != FALSE AND $permitted == FALSE) {
            $authData = array(
                'user_id' => $userId, 
                'page_id' => $pageId,
            );

            $result =  $this->db->insert($this->table->authorization, $authData);

            if ($result > 0) {
                $this->responseBody =  true;
            } else {
                $this->responseBody =  false;
            }
        } else {
            $this->responseBody =  false;
        }
        return $this->responseBody;
    }

    public function revokePermission($userId, $pageId)
    {
        $thePage = $this->getPage($pageId);
        $permitted = $this->hasPagePermission($userId, $pageId);

        if ($thePage != FALSE AND $permitted != FALSE) {

            $result =  $this->db->delete2("DELETE FROM ".$this->table->authorization." WHERE user_id = $userId AND page_id = $pageId");

            if ($result > 0) {
                $this->responseBody =  true;
            } else {
                $this->responseBody =  false;
            }
        } else {
            $this->responseBody =  false;
        }
        return $this->responseBody;
    }

}