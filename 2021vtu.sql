-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 18, 2022 at 04:04 AM
-- Server version: 5.7.37
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `generals_2021vtu`
--

-- --------------------------------------------------------

--
-- Table structure for table `beneficiary`
--

CREATE TABLE `beneficiary` (
  `id` int(11) NOT NULL,
  `beneficiary_name` varchar(50) DEFAULT NULL,
  `phone_number` varchar(15) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `beneficiary`
--

INSERT INTO `beneficiary` (`id`, `beneficiary_name`, `phone_number`, `user_id`) VALUES
(1, '', '09036830349', 13),
(2, NULL, '08119900857', 13);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `date`) VALUES
(1, 'Airtime Topup', '2021-03-10 15:12:02'),
(2, 'Data Bundle', '2021-03-10 15:12:02');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `location` enum('toolbar','quick_user','user_profile_aside') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `parent_id`, `role`, `location`) VALUES
(1, 'Dashboard', 'dashboard', NULL, '1,2,3', 'toolbar'),
(2, 'My Account', NULL, NULL, '1,2,3', 'toolbar'),
(3, 'Airtime Topup', 'airtime-topup', NULL, '1,2,3', 'toolbar'),
(4, 'Data Bundles', NULL, NULL, '1,2,3', 'toolbar'),
(5, 'Subscriptions', NULL, NULL, '1,2,3', 'toolbar'),
(6, 'Others', NULL, NULL, '1,2,3', 'toolbar'),
(7, 'Fund Wallet', 'wallet', 2, '1,2,3', 'toolbar'),
(8, 'Share Money', 'share-money', 2, '1,2,3', 'toolbar'),
(9, 'Commission', 'commissions', 2, '1,2,3', 'toolbar'),
(10, 'Price List', 'price-list', 2, '1,2,3', 'toolbar'),
(11, 'MTN Bundles', 'mtn-bundle', 4, '1,2,3', 'toolbar'),
(12, 'Other GSM Bundles', 'other-gsm-bundles', 4, '1,2,3', 'toolbar'),
(13, 'Broadband Bundle', 'broad-band-bundle', 4, '1,2,3', 'toolbar'),
(14, 'Cable Tv', 'cable-tv', 5, '1,2,3', 'toolbar'),
(15, 'Electricity', 'electricity', 5, '1,2,3', 'toolbar'),
(16, 'Bulk SMS', 'bulk-sms', 6, '1,2,3', 'toolbar'),
(17, 'WAEC Result Checker', 'waec-result-checker', 6, '1,2,3', 'toolbar');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `module_name` varchar(50) NOT NULL,
  `module_page_url` varchar(255) DEFAULT NULL,
  `module_group` varchar(50) NOT NULL,
  `type` enum('user','admin') NOT NULL,
  `location` enum('toolbar','quick_user','user_profile_aside') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_name`, `module_page_url`, `module_group`, `type`, `location`) VALUES
(1, 'Home', 'home', 'Home', 'user', 'toolbar'),
(3, 'Airtime Topup', 'airtime-topup', 'My Account', 'user', 'toolbar'),
(7, 'Fund Wallet', 'wallet', 'My Account', 'user', 'toolbar'),
(8, 'Share Money', 'share-money', 'My Account', 'user', 'toolbar'),
(9, 'Commission', 'commissions', 'My Account', 'user', 'toolbar'),
(10, 'Price List', 'price-list', 'My Account', 'user', 'toolbar'),
(11, 'MTN Bundles', 'mtn-bundle', 'Data Bundles', 'user', 'toolbar'),
(12, 'Other GSM Bundles', 'other-gsm-bundles', 'Data Bundles', 'user', 'toolbar'),
(13, 'Broadband Bundle', 'broad-band-bundle', 'Data Bundles', 'user', 'toolbar'),
(14, 'Cable Tv', 'cable-tv', 'Subscriptions', 'user', 'toolbar'),
(15, 'Electricity', 'electricity', 'Subscriptions', 'user', 'toolbar'),
(16, 'Bulk SMS', 'bulk-sms', 'Others', 'user', 'toolbar'),
(17, 'WAEC Result Checker', 'waec-result-checker', 'Others', 'user', 'toolbar'),
(18, 'Member List', 'member-list', 'User Management', 'admin', 'toolbar'),
(19, 'Member Info', 'member-info', 'User Management', 'admin', 'toolbar'),
(20, 'Member Stats', 'member-stats', 'User Management', 'admin', 'toolbar'),
(21, 'SMS Member', 'sms-member', 'User Management', 'admin', 'toolbar'),
(22, 'Products', 'products', 'System Management', 'admin', 'toolbar'),
(23, 'Role Management', 'role-management', 'System Management', 'admin', 'toolbar'),
(24, 'Plan Management', 'plan-management', 'System Management', 'admin', 'toolbar'),
(25, 'System Settings', 'system-settings', 'System Management', 'admin', 'toolbar'),
(26, 'Authorization', 'authorization', 'System Management', 'admin', 'toolbar'),
(27, 'SMS Setting', 'sms-setting', 'System Management', 'admin', 'toolbar'),
(29, 'Dashboard', 'dashboard', 'Dashboard', 'admin', 'toolbar');

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `plan_name` varchar(50) NOT NULL,
  `migration_fee` double NOT NULL,
  `referral_fee` double NOT NULL DEFAULT '0',
  `allow_referral` tinyint(1) NOT NULL DEFAULT '1',
  `sales_target` int(11) DEFAULT NULL,
  `plan_lock` tinyint(1) NOT NULL,
  `purchase_limit` int(11) DEFAULT NULL,
  `plan_type` enum('private','public') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `plan_name`, `migration_fee`, `referral_fee`, `allow_referral`, `sales_target`, `plan_lock`, `purchase_limit`, `plan_type`, `created_by`, `created_date`, `status`) VALUES
(1, 'Big Zone', 1200, 0, 1, NULL, 1, NULL, 'private', 13, '2021-03-25 09:22:31', 1),
(2, 'Entry zone', 0, 0, 1, NULL, 0, NULL, 'public', 13, '2021-03-30 12:36:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `category` varchar(20) NOT NULL,
  `cost_price` double DEFAULT NULL,
  `company_price` double NOT NULL,
  `product_icon` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_code`, `category`, `cost_price`, `company_price`, `product_icon`, `status`, `date`) VALUES
(1, '9Mobile 4.5GB/30days', '9mobile4500', 'Data Bundle', 2000, 2000, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(2, '9Mobile 2.0GB/30days', '9mobile2000', 'Data Bundle', 1200, 1200, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(3, '9Mobile 1.5GB/30days', '9mobile1500', 'Data Bundle', 980, 1000, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(4, '9Mobile 3.0GB/30days', '9mobile3000', 'Data Bundle', 1500, 1500, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(5, 'Airtel 4.5GB/30days', 'airtel4500', 'Data Bundle', 1960, 2000, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(6, 'Airtel 1.5GB/30days', 'airtel1500', 'Data Bundle', 980, 1000, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(7, 'GLO 6.25GB/750MB Night 30Days', 'gloid05', 'Data Bundle', 2500, 2500, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(8, 'GLO 8.25GB/750MB Night 30Days', 'gloid06', 'Data Bundle', 3000, 3000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(9, 'GLO 11.25GB/750MB Night 30Days', 'gloid07', 'Data Bundle', 4000, 4000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(10, 'GLO 15.5GB/1GB Night 30Days', 'gloid08', 'Data Bundle', 5000, 5000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(11, 'GLO 24GB/1GB Night 30Days', 'gloid09', 'Data Bundle', 8000, 8000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(12, 'Smile 100GB Bigga', 'smile100gbbigga', 'Broadband data', 18000, 18000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(13, 'GLO 40GB/2GB Night 30Days', 'gloid10', 'Data Bundle', 10000, 10000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(14, 'GLO 75GB/3GB Night 30Days', 'gloid11', 'Data Bundle', 15000, 15000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(15, 'GLO 95GB/5GB Night 30Days', 'gloid12', 'Data Bundle', 18000, 18000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(16, 'GLO 110GB/5GB Night 30Days', 'gloid13', 'Data Bundle', 20000, 20000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(17, 'GLO 1.8GB/500MB Night 30Days', 'gloid02', 'Data Bundle', 1000, 1000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(18, 'GLO 800MB/200MB Night 14days', 'gloid01', 'Data Bundle', 500, 500, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(19, 'MTN 5.0GB/30days SME', 'mtn5000sme', 'Data Bundle', 1950, 5000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(20, 'MTN 2.0GB/30days SME', 'mtn2000sme', 'Data Bundle', 780, 2000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(21, 'MTN 1.0GB/30days SME', 'mtn1000sme', 'Data Bundle', 390, 1000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(22, 'Non DND Route', 'nondndsms', 'BulkSMS', 2, 100, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(23, 'DND Route', 'dndsms', 'BulkSMS', 3.25, 100, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(24, '9Mobile', '9mobile', 'Airtime Topup', 0, 100, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(25, 'Airtel', 'airtel', 'Airtime Topup', 0, 100, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(26, 'GLO', 'glo', 'Airtime Topup', 0, 100, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(27, 'MTN', 'mtn', 'Airtime Topup', 0, 100, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(28, '9Mobile 75.0GB/30days', '9mobile75000', 'Data Bundle', 15000, 15000, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(29, '9Mobile 11.0GB/30days', '9mobile11000', 'Data Bundle', 4000, 4000, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(30, '9Mobile 15.0GB/30days', '9mobile15000', 'Data Bundle', 4900, 5000, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(31, '9Mobile 40.0GB/30days', '9mobile40000', 'Data Bundle', 10000, 10000, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(32, 'Star Times Nova', 'startimesnova', 'Cable TV', 940, 900, 'uploads/startimes.png', 1, '2021-03-23 15:51:35'),
(33, 'Star Times Basic', 'startimesbasic', 'Cable TV', 1740, 1700, 'uploads/startimes.png', 1, '2021-03-23 15:51:35'),
(34, 'Star Times Smart', 'startimessmart', 'Cable TV', 2240, 2200, 'uploads/startimes.png', 1, '2021-03-23 15:51:35'),
(35, 'Star Times Classic', 'startimesclassic', 'Cable TV', 2540, 2500, 'uploads/startimes.png', 1, '2021-03-23 15:51:35'),
(36, 'Star Times Super', 'startimessuper', 'Cable TV', 4240, 4200, 'uploads/startimes.png', 1, '2021-03-23 15:51:35'),
(37, 'DSTV Padi', 'dstvpadi', 'Cable TV', 1850, 1850, 'uploads/dstv.jpg', 1, '2021-03-23 15:51:35'),
(38, 'DSTV Compact', 'dstvcompact', 'Cable TV', 7940, 7900, 'uploads/dstv.jpg', 1, '2021-03-23 15:51:35'),
(39, 'DSTV Compact Plus', 'dstvcompactplus', 'Cable TV', 12440, 12400, 'uploads/dstv.jpg', 1, '2021-03-23 15:51:35'),
(40, 'DSTV Premium', 'dstvpremium', 'Cable TV', 18440, 18400, 'uploads/dstv.jpg', 1, '2021-03-23 15:51:35'),
(41, 'Smile 400GB-Anytime', 'smile400gbanytime', 'Broadband data', 120000, 120000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(42, 'Smile UnlimitedPremium', 'smileunlimitedpremium', 'Broadband data', 20000, 20000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(43, 'Smile UnlimitedPlatinum', 'smileunlimitedplatinum', 'Broadband data', 24000, 24000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(44, 'Gotv Max', 'gotvmax', 'Cable TV', 3640, 3600, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(45, 'IBEDC Prepaid', 'ibedcprepd', 'Electricity Bills', 40, 100, 'uploads/ibdec.jpg', 1, '2021-03-23 15:51:35'),
(47, 'Ikeja Prepaid', 'ikejaprepd', 'Electricity Bills', 40, 100, 'uploads/ikedc.jpg', 1, '2021-03-23 15:51:35'),
(49, 'Eko Prepaid', 'ekoprepd', 'Electricity Bills', 40, 100, 'uploads/ekedc.jpg', 1, '2021-03-23 15:51:35'),
(51, 'Port Harcourt Prepaid', 'phedcprepd', 'Electricity Bills', 40, 100, 'uploads/phedc.jpg', 1, '2021-03-23 15:51:35'),
(53, 'Smile 35GB-Anytime', 'smile35gbanytime', 'Broadband data', 16000, 16000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(54, 'Smile Unlimited-Essential', 'smileunlimitedessential', 'Broadband data', 15000, 15000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(55, 'Smile 50GB Bumpa-Value', 'smile50gbbumpavalue', 'Broadband data', 15000, 15000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(56, 'Smile 75GB Bigga', 'smile75gbbigga', 'Broadband data', 15000, 15000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(57, 'Smile Unlimited-Lite', 'smileunlimitedlite', 'Broadband data', 10000, 10000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(58, 'Smile 60GB Bigga', 'smile60gbbigga', 'Broadband data', 13500, 13500, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(59, 'Smile 40GB Bigga', 'smile40gbbigga', 'Broadband data', 10000, 10000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(60, 'Smile 30GB Bigga', 'smile30gbbigga', 'Broadband data', 8000, 8000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(61, 'Smile 20GB Bigga', 'smile20gbbigga', 'Broadband data', 6000, 6000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(62, 'Smile 15GB-Anytime', 'smile15GBanytime', 'Broadband data', 8000, 8000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(63, 'Smile 15GB Bigga', 'smile15gbbigga', 'Broadband data', 5000, 5000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(64, 'Smile 12GB Bigga', 'smile12gbbigga', 'Broadband data', 4000, 4000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(65, 'Smile 10GB Bigga', 'smile10gbbigga', 'Broadband data', 3500, 3500, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(66, 'Smile 8GB Bigga', 'smile8gbbigga', 'Broadband data', 3000, 3000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(67, 'SmileVoice ONLY 430', 'smilevoiceonly430', 'Broadband data', 3070, 3070, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(68, 'Smile 6.5GB Bigga', 'smile6500mbbigga', 'Broadband data', 2500, 2500, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(69, 'Smile 5GB Bigga', 'smile5gbbigga', 'Broadband data', 2000, 2000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(70, 'Smile 3GB Weekend ONLY', 'smile3gbweekendonly', 'Broadband data', 1530, 1530, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(71, 'Smile 3GB MidNite', 'smile3gbmidnite', 'Broadband data', 1530, 1530, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(72, 'Smile 3GB Bigga', 'smile3gbbigga', 'Broadband data', 1500, 1500, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(73, 'Smile 6GB Flexi-Weekly', 'smile6gbflexiweekly', 'Broadband data', 1500, 1500, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(74, 'Smile 2GB Bigga', 'smile2gbbigga', 'Broadband data', 1200, 1200, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(75, 'Smile 2GB MidNite', 'smile2gbmidnite', 'Broadband data', 1020, 1020, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(76, 'Smile 2GB Flexi-Weekly', 'smile2gbflexiweekly', 'Broadband data', 1000, 1000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(77, 'SmileVoice ONLY 135', 'smilevoiceonly135', 'Broadband data', 1020, 1020, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(78, 'MTN DataPIN 1.0GB', 'mtndatapin1k', 'DataPIN', 400, 1000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(79, 'MTN DataPIN 5.0GB', 'mtndatapin5k', 'DataPIN', 2000, 5000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(80, 'MTN DataPIN 2.0GB', 'mtndatapin2k', 'DataPIN', 800, 2000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(81, 'Smile 200GB-Anytime', 'smile200gbanytime', 'Broadband data', 70000, 70000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(82, 'Smile 100GB Bumpa-Value', 'smile100gbbumpavalue', 'Broadband data', 40000, 40000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(83, 'Smile 90GB-Anytime', 'smile90gbanytime', 'Broadband data', 36000, 36000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(84, 'Smile 80GB Bumpa-Value', 'smile80gbpumpavalue', 'Broadband data', 30000, 30000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(85, 'Premium Route', 'premiumsms', 'BulkSMS', 0, 100, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(86, 'WAEC PIN', 'waec', 'Education Pay', 1850, 2000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(87, 'MTN 1GB/1day Direct', 'mtn1gb24hrsdirect', 'Data Bundle', 297, 300, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(88, '9Mobile 500MB/30days', '9mobile500', 'Data Bundle', 490, 500, 'uploads/9mobile.jpg', 1, '2021-03-23 15:51:35'),
(89, 'MTN 350MB/7days Direct', 'mtn350mb7daysdirect', 'Data Bundle', 297, 300, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(90, 'MTN 750MB/14days Direct', 'mtn750mb14daysdirect', 'Data Bundle', 495, 500, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(91, 'MTN 1.5GB/30days Direct', 'mtn1500mb30daysdirect', 'Data Bundle', 990, 1000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(92, 'MTN 2GB/30days Direct', 'mtn2000mb30daysdirect', 'Data Bundle', 1188, 1200, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(93, 'MTN 110GB/30days Direct', 'mtn110000mb30daysdirect', 'Data Bundle', 19800, 20000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(94, 'MTN 40GB/30days Direct', 'mtn40000mb30daysdirect', 'Data Bundle', 9900, 10000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(95, 'DSTV Asian Add-on', 'asianaddon', 'Cable TV', 5540, 5540, 'uploads/dstv.jpg', 1, '2021-03-23 15:51:35'),
(96, 'Airtel 6.0GB/30days', 'airtel6000', 'Data Bundle', 2450, 2500, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(97, 'MTN 6GB/30days Direct', 'mtn6000mb30daysdirect', 'Data Bundle', 2475, 2500, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(98, 'MTN 15GB/30days Direct', 'mtn11000mb30daysdirect', 'Data Bundle', 4950, 5000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(99, 'MTN 10GB/30days Direct', 'mtn6500mb30daysdirect', 'Data Bundle', 3465, 3500, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(100, 'MTN 4.5GB/30days Direct', 'mtn3500mb30daysdirect', 'Data Bundle', 1980, 2000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(101, 'MTN 1GB/Weekly Direct', 'mtn1000mbweeklydirect', 'Data Bundle', 495, 500, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(102, 'MTN 6GB/Weekly Direct', 'mtn6000mbweeklydirect', 'Data Bundle', 1485, 1500, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(103, 'Abuja Prepaid', 'aedcprepd', 'Electricity Bills', 0, 100, 'uploads/aedc.jpg', 1, '2021-03-23 15:51:35'),
(105, 'Enugu Prepaid', 'eedcprepd', 'Electricity Bills', 0, 100, 'uploads/eedc.png', 1, '2021-03-23 15:51:35'),
(107, 'Jos Prepaid', 'jedcprepd', 'Electricity Bills', 0, 100, 'uploads/jedc.jpg', 1, '2021-03-23 15:51:35'),
(109, 'Kaduna Prepaid', 'kaedcprepd', 'Electricity Bills', 0, 100, 'uploads/kadedc.jpg', 1, '2021-03-23 15:51:35'),
(111, 'Kano Prepaid', 'kedcprepd', 'Electricity Bills', 0, 100, 'uploads/kedco.jpg', 1, '2021-03-23 15:51:35'),
(113, 'GOtv Jinja', 'gotvjinja', 'Cable TV', 1680, 1640, 'uploads/gotv.jpg', 1, '2021-03-23 15:51:35'),
(115, 'DSTV Yanga', 'dstvyanga', 'Cable TV', 2605, 2565, 'uploads/dstv.jpg', 1, '2021-03-23 15:51:35'),
(116, 'DSTV Confam', 'dstvconfam', 'Cable TV', 4655, 4615, 'uploads/dstv.jpg', 1, '2021-03-23 15:51:35'),
(117, 'GLO 4.75GB/500MB Night 30Days', 'gloid04', 'Data Bundle', 2000, 2000, 'uploads/glo1.png', 1, '2021-03-23 15:51:35'),
(118, 'Smile 1.5GB Bigga', 'smile1500mbbigga', 'Broadband data', 1000, 1000, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(119, 'SmileVoice ONLY 65', 'smilevoiceonly65', 'Broadband data', 510, 510, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(120, 'Smile 1GB Flexi-Weekly', 'smile1gbflexiweekly', 'Broadband data', 500, 500, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(121, 'Smile 1GB Flexi', 'smile1gbflexi', 'Broadband data', 300, 300, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(122, 'Smile 2.5GB Flexi', 'smile2500mbflexi', 'Broadband data', 500, 500, 'uploads/smile.jpg', 1, '2021-03-23 15:51:35'),
(123, 'Airtel 2.0GB/30days', 'airtel2000', 'Data Bundle', 1176, 1200, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(124, 'Airtel 3.0GB/30days', 'airtel3000', 'Data Bundle', 1500, 1500, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(125, 'DSTV HDPVR/XtraView Add-on', 'extraviewaddon', 'Cable TV', 2500, 2500, 'uploads/dstv.jpg', 1, '2021-03-23 15:51:35'),
(126, 'MTN 400GB/365days Direct', 'mtn400000mb365daysdirect', 'Data Bundle', 118800, 120000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(127, 'MTN 250GB/90days Direct', 'mtn250000mb90daysdirect', 'Data Bundle', 74250, 75000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(128, 'MTN 150GB/90days Direct', 'mtn150000mb90daysdirect', 'Data Bundle', 49500, 50000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(129, 'MTN 120GB/60days Direct', 'mtn120000mb60daysdirect', 'Data Bundle', 29700, 30000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(130, 'MTN 75GB/60days Direct', 'mtn75000mb60daysdirect', 'Data Bundle', 19800, 20000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(131, 'MTN 8GB/30days Direct', 'mtn8000mb30daysdirect', 'Data Bundle', 2970, 3000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(132, 'MTN 2.5GB/2days Direct', 'mtn2500mb2daysdirect', 'Data Bundle', 495, 500, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(133, 'MTN 200MB/1day Direct', 'mtn200mb24hrsdirect', 'Data Bundle', 198, 200, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(134, 'MTN 100MB/1day Direct', 'mtn100mb24hrsdirect', 'Data Bundle', 99, 100, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(135, 'Airtel 8.0GB/30days', 'airtel8000mb30days', 'Data Bundle', 3000, 3000, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(136, 'Airtel 11.0GB/30days', 'airtel11000mb30days', 'Data Bundle', 4000, 4000, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(137, 'Airtel 15.0GB/30days', 'airtel15000mb30days', 'Data Bundle', 5000, 5000, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(138, 'Airtel 40.0GB/30days', 'airtel40000mb30days', 'Data Bundle', 10000, 10000, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(139, 'Airtel 75.0GB/30days', 'airtel75000mb30days', 'Data Bundle', 15000, 15000, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(140, 'Airtel 110.0GB/30days', 'airtel110000mb30days', 'Data Bundle', 20000, 20000, 'uploads/airtel1.png', 1, '2021-03-23 15:51:35'),
(141, 'GOtv Smallie', 'gotvsmallie', 'Cable TV', 840, 800, 'uploads/gotv.jpg', 1, '2021-03-23 15:51:35'),
(142, 'MTN 1.0GB/30days Gifting', 'mtngift1000', 'Data Bundle', 0, 1000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(143, 'MTN 2.0GB/30days Gifting', 'mtngift2000', 'Data Bundle', 0, 1000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(144, 'MTN 3.0GB/30days Gifting', 'mtngift3000', 'Data Bundle', 0, 2000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(145, 'MTN 5.0GB/30days Gifting', 'mtngift5000', 'Data Bundle', 0, 3000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(146, 'MTN 15.0GB/30days Gifting', 'mtngift15000', 'Data Bundle', 0, 10000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(147, 'MTN 500MB/30days Gifting', 'mtngift500', 'Data Bundle', 0, 1000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(148, 'MTN 20.0GB/30days Gifting', 'mtngift20000', 'Data Bundle', 0, 10000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(149, 'MTN 40.0GB/30days Gifting', 'mtngift40000', 'Data Bundle', 0, 15000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35'),
(150, 'MTN 10.0GB/30days Gifting', 'mtngift10000', 'Data Bundle', 0, 5000, 'uploads/mtn1.png', 1, '2021-03-23 15:51:35');

-- --------------------------------------------------------

--
-- Table structure for table `product_plan`
--

CREATE TABLE `product_plan` (
  `id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `selling_percentage` double NOT NULL DEFAULT '0',
  `extra_charge` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_plan`
--

INSERT INTO `product_plan` (`id`, `product_code`, `plan_id`, `selling_percentage`, `extra_charge`) VALUES
(1, '9mobile4500', 1, 90, 87),
(2, '9mobile2000', 1, 87, 0),
(3, '9mobile1500', 1, 0, 0),
(4, '9mobile3000', 1, 70, 0),
(5, 'airtel4500', 1, 90, 0),
(6, 'airtel1500', 1, 0, 0),
(7, 'gloid05', 1, 0, 0),
(8, 'gloid06', 1, 0, 0),
(9, 'gloid07', 1, 0, 0),
(10, 'gloid08', 1, 0, 0),
(11, 'gloid09', 1, 0, 0),
(12, 'smile100gbbigga', 1, 0, 0),
(13, 'gloid10', 1, 0, 0),
(14, 'gloid11', 1, 0, 0),
(15, 'gloid12', 1, 0, 0),
(16, 'gloid13', 1, 0, 0),
(17, 'gloid02', 1, 0, 0),
(18, 'gloid01', 1, 0, 0),
(19, 'mtn5000sme', 1, 0, 0),
(20, 'mtn2000sme', 1, 0, 0),
(21, 'mtn1000sme', 1, 0, 0),
(22, 'nondndsms', 1, 0, 0),
(23, 'dndsms', 1, 0, 0),
(24, '9mobile', 1, 93, 0),
(25, 'airtel', 1, 93, 0),
(26, 'glo', 1, 93, 0),
(27, 'mtn', 1, 93, 0),
(28, '9mobile75000', 1, 0, 0),
(29, '9mobile11000', 1, 0, 0),
(30, '9mobile15000', 1, 0, 0),
(31, '9mobile40000', 1, 0, 0),
(32, 'startimesnova', 1, 0, 0),
(33, 'startimesbasic', 1, 0, 0),
(34, 'startimessmart', 1, 0, 0),
(35, 'startimesclassic', 1, 0, 0),
(36, 'startimessuper', 1, 0, 0),
(37, 'dstvpadi', 1, 0, 0),
(38, 'dstvcompact', 1, 0, 0),
(39, 'dstvcompactplus', 1, 0, 0),
(40, 'dstvpremium', 1, 0, 0),
(41, 'smile400gbanytime', 1, 0, 0),
(42, 'smileunlimitedpremium', 1, 0, 0),
(43, 'smileunlimitedplatinum', 1, 0, 0),
(44, 'gotvmax', 1, 0, 0),
(45, 'ibedcprepd', 1, 0, 0),
(46, 'ibedcpostpd', 1, 48, 0),
(47, 'ikejaprepd', 1, 0, 0),
(48, 'ikejapostpd', 1, 89, 0),
(49, 'ekoprepd', 1, 0, 0),
(50, 'ekopostpd', 1, 90, 0),
(51, 'phedcprepd', 1, 0, 0),
(52, 'phedcpostpd', 1, 70, 0),
(53, 'smile35gbanytime', 1, 0, 0),
(54, 'smileunlimitedessential', 1, 0, 0),
(55, 'smile50gbbumpavalue', 1, 0, 0),
(56, 'smile75gbbigga', 1, 0, 0),
(57, 'smileunlimitedlite', 1, 0, 0),
(58, 'smile60gbbigga', 1, 0, 0),
(59, 'smile40gbbigga', 1, 0, 0),
(60, 'smile30gbbigga', 1, 0, 0),
(61, 'smile20gbbigga', 1, 0, 0),
(62, 'smile15GBanytime', 1, 0, 0),
(63, 'smile15gbbigga', 1, 0, 0),
(64, 'smile12gbbigga', 1, 0, 0),
(65, 'smile10gbbigga', 1, 0, 0),
(66, 'smile8gbbigga', 1, 0, 0),
(67, 'smilevoiceonly430', 1, 0, 0),
(68, 'smile6500mbbigga', 1, 0, 0),
(69, 'smile5gbbigga', 1, 0, 0),
(70, 'smile3gbweekendonly', 1, 0, 0),
(71, 'smile3gbmidnite', 1, 0, 0),
(72, 'smile3gbbigga', 1, 0, 0),
(73, 'smile6gbflexiweekly', 1, 0, 0),
(74, 'smile2gbbigga', 1, 0, 0),
(75, 'smile2gbmidnite', 1, 0, 0),
(76, 'smile2gbflexiweekly', 1, 0, 0),
(77, 'smilevoiceonly135', 1, 0, 0),
(78, 'mtndatapin1k', 1, 0, 0),
(79, 'mtndatapin5k', 1, 0, 0),
(80, 'mtndatapin2k', 1, 0, 0),
(81, 'smile200gbanytime', 1, 0, 0),
(82, 'smile100gbbumpavalue', 1, 0, 0),
(83, 'smile90gbanytime', 1, 0, 0),
(84, 'smile80gbpumpavalue', 1, 0, 0),
(85, 'premiumsms', 1, 0, 0),
(86, 'waec', 1, 0, 0),
(87, 'mtn1gb24hrsdirect', 1, 0, 0),
(88, '9mobile500', 1, 0, 0),
(89, 'mtn350mb7daysdirect', 1, 0, 0),
(90, 'mtn750mb14daysdirect', 1, 0, 0),
(91, 'mtn1500mb30daysdirect', 1, 0, 0),
(92, 'mtn2000mb30daysdirect', 1, 0, 0),
(93, 'mtn110000mb30daysdirect', 1, 0, 0),
(94, 'mtn40000mb30daysdirect', 1, 0, 0),
(95, 'asianaddon', 1, 0, 0),
(96, 'airtel6000', 1, 0, 0),
(97, 'mtn6000mb30daysdirect', 1, 0, 0),
(98, 'mtn11000mb30daysdirect', 1, 0, 9),
(99, 'mtn6500mb30daysdirect', 1, 98, 800),
(100, 'mtn3500mb30daysdirect', 1, 0, 0),
(101, 'mtn1000mbweeklydirect', 1, 0, 0),
(102, 'mtn6000mbweeklydirect', 1, 0, 0),
(103, 'aedcprepd', 1, 0, 0),
(104, 'aedcpostpd', 1, 100, 0),
(105, 'eedcprepd', 1, 0, 0),
(106, 'eedcpostpd', 1, 90, 0),
(107, 'jedcprepd', 1, 0, 0),
(108, 'jedcpostpd', 1, 0, 0),
(109, 'kaedcprepd', 1, 0, 0),
(110, 'kaedcpostpd', 1, 0, 0),
(111, 'kedcprepd', 1, 0, 0),
(112, 'kedcpostpd', 1, 0, 0),
(113, 'gotvjinja', 1, 0, 0),
(114, 'gotvjolli', 1, 0, 0),
(115, 'dstvyanga', 1, 0, 0),
(116, 'dstvconfam', 1, 0, 0),
(117, 'gloid04', 1, 0, 0),
(118, 'smile1500mbbigga', 1, 0, 0),
(119, 'smilevoiceonly65', 1, 0, 0),
(120, 'smile1gbflexiweekly', 1, 0, 0),
(121, 'smile1gbflexi', 1, 0, 0),
(122, 'smile2500mbflexi', 1, 0, 0),
(123, 'airtel2000', 1, 0, 0),
(124, 'airtel3000', 1, 0, 0),
(125, 'extraviewaddon', 1, 0, 0),
(126, 'mtn400000mb365daysdirect', 1, 0, 0),
(127, 'mtn250000mb90daysdirect', 1, 0, 0),
(128, 'mtn150000mb90daysdirect', 1, 0, 0),
(129, 'mtn120000mb60daysdirect', 1, 0, 0),
(130, 'mtn75000mb60daysdirect', 1, 0, 0),
(131, 'mtn8000mb30daysdirect', 1, 0, 0),
(132, 'mtn2500mb2daysdirect', 1, 0, 0),
(133, 'mtn200mb24hrsdirect', 1, 0, 0),
(134, 'mtn100mb24hrsdirect', 1, 0, 0),
(135, 'airtel8000mb30days', 1, 0, 0),
(136, 'airtel11000mb30days', 1, 0, 0),
(137, 'airtel15000mb30days', 1, 0, 0),
(138, 'airtel40000mb30days', 1, 0, 0),
(139, 'airtel75000mb30days', 1, 0, 0),
(140, 'airtel110000mb30days', 1, 0, 0),
(141, 'gotvsmallie', 1, 0, 0),
(142, 'mtngift1000', 1, 0, 0),
(143, 'mtngift2000', 1, 0, 0),
(144, 'mtngift3000', 1, 0, 0),
(145, 'mtngift5000', 1, 0, 0),
(146, 'mtngift15000', 1, 0, 0),
(147, 'mtngift500', 1, 0, 0),
(148, 'mtngift20000', 1, 0, 0),
(149, 'mtngift40000', 1, 0, 0),
(150, 'mtngift10000', 1, 0, 0),
(151, '9mobile4500', 2, 90, 87),
(152, '9mobile2000', 2, 87, 0),
(153, '9mobile1500', 2, 0, 0),
(154, '9mobile3000', 2, 70, 0),
(155, 'airtel4500', 2, 90, 0),
(156, 'airtel1500', 2, 0, 0),
(157, 'gloid05', 2, 0, 0),
(158, 'gloid06', 2, 0, 0),
(159, 'gloid07', 2, 0, 0),
(160, 'gloid08', 2, 0, 0),
(161, 'gloid09', 2, 0, 0),
(162, 'smile100gbbigga', 2, 0, 0),
(163, 'gloid10', 2, 0, 0),
(164, 'gloid11', 2, 0, 0),
(165, 'gloid12', 2, 0, 0),
(166, 'gloid13', 2, 0, 0),
(167, 'gloid02', 2, 0, 0),
(168, 'gloid01', 2, 0, 0),
(169, 'mtn5000sme', 2, 0, 0),
(170, 'mtn2000sme', 2, 0, 0),
(171, 'mtn1000sme', 2, 0, 0),
(172, 'nondndsms', 2, 0, 0),
(173, 'dndsms', 2, 0, 0),
(174, '9mobile', 2, 93, 0),
(175, 'airtel', 2, 93, 0),
(176, 'glo', 2, 93, 0),
(177, 'mtn', 2, 93, 0),
(178, '9mobile75000', 2, 0, 0),
(179, '9mobile11000', 2, 0, 0),
(180, '9mobile15000', 2, 0, 0),
(181, '9mobile40000', 2, 0, 0),
(182, 'startimesnova', 2, 0, 0),
(183, 'startimesbasic', 2, 0, 0),
(184, 'startimessmart', 2, 0, 0),
(185, 'startimesclassic', 2, 0, 0),
(186, 'startimessuper', 2, 0, 0),
(187, 'dstvpadi', 2, 0, 0),
(188, 'dstvcompact', 2, 0, 0),
(189, 'dstvcompactplus', 2, 0, 0),
(190, 'dstvpremium', 2, 0, 0),
(191, 'smile400gbanytime', 2, 0, 0),
(192, 'smileunlimitedpremium', 2, 0, 0),
(193, 'smileunlimitedplatinum', 2, 0, 0),
(194, 'gotvmax', 2, 0, 0),
(195, 'ibedcprepd', 2, 0, 0),
(196, 'ibedcpostpd', 2, 0, 0),
(197, 'ikejaprepd', 2, 0, 0),
(198, 'ikejapostpd', 2, 0, 0),
(199, 'ekoprepd', 2, 0, 0),
(200, 'ekopostpd', 2, 0, 0),
(201, 'phedcprepd', 2, 0, 0),
(202, 'phedcpostpd', 2, 0, 0),
(203, 'smile35gbanytime', 2, 0, 0),
(204, 'smileunlimitedessential', 2, 0, 0),
(205, 'smile50gbbumpavalue', 2, 0, 0),
(206, 'smile75gbbigga', 2, 0, 0),
(207, 'smileunlimitedlite', 2, 0, 0),
(208, 'smile60gbbigga', 2, 0, 0),
(209, 'smile40gbbigga', 2, 0, 0),
(210, 'smile30gbbigga', 2, 0, 0),
(211, 'smile20gbbigga', 2, 0, 0),
(212, 'smile15GBanytime', 2, 0, 0),
(213, 'smile15gbbigga', 2, 0, 0),
(214, 'smile12gbbigga', 2, 0, 0),
(215, 'smile10gbbigga', 2, 0, 0),
(216, 'smile8gbbigga', 2, 0, 0),
(217, 'smilevoiceonly430', 2, 0, 0),
(218, 'smile6500mbbigga', 2, 0, 0),
(219, 'smile5gbbigga', 2, 0, 0),
(220, 'smile3gbweekendonly', 2, 0, 0),
(221, 'smile3gbmidnite', 2, 0, 0),
(222, 'smile3gbbigga', 2, 0, 0),
(223, 'smile6gbflexiweekly', 2, 0, 0),
(224, 'smile2gbbigga', 2, 0, 0),
(225, 'smile2gbmidnite', 2, 0, 0),
(226, 'smile2gbflexiweekly', 2, 0, 0),
(227, 'smilevoiceonly135', 2, 0, 0),
(228, 'mtndatapin1k', 2, 0, 0),
(229, 'mtndatapin5k', 2, 0, 0),
(230, 'mtndatapin2k', 2, 0, 0),
(231, 'smile200gbanytime', 2, 0, 0),
(232, 'smile100gbbumpavalue', 2, 0, 0),
(233, 'smile90gbanytime', 2, 0, 0),
(234, 'smile80gbpumpavalue', 2, 0, 0),
(235, 'premiumsms', 2, 0, 0),
(236, 'waec', 2, 0, 0),
(237, 'mtn1gb24hrsdirect', 2, 0, 0),
(238, '9mobile500', 2, 0, 0),
(239, 'mtn350mb7daysdirect', 2, 0, 0),
(240, 'mtn750mb14daysdirect', 2, 0, 0),
(241, 'mtn1500mb30daysdirect', 2, 0, 0),
(242, 'mtn2000mb30daysdirect', 2, 0, 0),
(243, 'mtn110000mb30daysdirect', 2, 0, 0),
(244, 'mtn40000mb30daysdirect', 2, 0, 0),
(245, 'asianaddon', 2, 0, 0),
(246, 'airtel6000', 2, 0, 0),
(247, 'mtn6000mb30daysdirect', 2, 0, 0),
(248, 'mtn11000mb30daysdirect', 2, 0, 9),
(249, 'mtn6500mb30daysdirect', 2, 98, 800),
(250, 'mtn3500mb30daysdirect', 2, 0, 0),
(251, 'mtn1000mbweeklydirect', 2, 0, 0),
(252, 'mtn6000mbweeklydirect', 2, 0, 0),
(253, 'aedcprepd', 2, 0, 0),
(254, 'aedcpostpd', 2, 0, 0),
(255, 'eedcprepd', 2, 0, 0),
(256, 'eedcpostpd', 2, 0, 0),
(257, 'jedcprepd', 2, 0, 0),
(258, 'jedcpostpd', 2, 0, 0),
(259, 'kaedcprepd', 2, 0, 0),
(260, 'kaedcpostpd', 2, 0, 0),
(261, 'kedcprepd', 2, 0, 0),
(262, 'kedcpostpd', 2, 0, 0),
(263, 'gotvjinja', 2, 0, 0),
(264, 'gotvjolli', 2, 0, 0),
(265, 'dstvyanga', 2, 0, 0),
(266, 'dstvconfam', 2, 0, 0),
(267, 'gloid04', 2, 0, 0),
(268, 'smile1500mbbigga', 2, 0, 0),
(269, 'smilevoiceonly65', 2, 0, 0),
(270, 'smile1gbflexiweekly', 2, 0, 0),
(271, 'smile1gbflexi', 2, 0, 0),
(272, 'smile2500mbflexi', 2, 0, 0),
(273, 'airtel2000', 2, 0, 0),
(274, 'airtel3000', 2, 0, 0),
(275, 'extraviewaddon', 2, 0, 0),
(276, 'mtn400000mb365daysdirect', 2, 0, 0),
(277, 'mtn250000mb90daysdirect', 2, 0, 0),
(278, 'mtn150000mb90daysdirect', 2, 0, 0),
(279, 'mtn120000mb60daysdirect', 2, 0, 0),
(280, 'mtn75000mb60daysdirect', 2, 0, 0),
(281, 'mtn8000mb30daysdirect', 2, 0, 0),
(282, 'mtn2500mb2daysdirect', 2, 0, 0),
(283, 'mtn200mb24hrsdirect', 2, 0, 0),
(284, 'mtn100mb24hrsdirect', 2, 0, 0),
(285, 'airtel8000mb30days', 2, 0, 0),
(286, 'airtel11000mb30days', 2, 0, 0),
(287, 'airtel15000mb30days', 2, 0, 0),
(288, 'airtel40000mb30days', 2, 0, 0),
(289, 'airtel75000mb30days', 2, 0, 0),
(290, 'airtel110000mb30days', 2, 0, 0),
(291, 'gotvsmallie', 2, 0, 0),
(292, 'mtngift1000', 2, 0, 0),
(293, 'mtngift2000', 2, 0, 0),
(294, 'mtngift3000', 2, 0, 0),
(295, 'mtngift5000', 2, 0, 0),
(296, 'mtngift15000', 2, 0, 0),
(297, 'mtngift500', 2, 0, 0),
(298, 'mtngift20000', 2, 0, 0),
(299, 'mtngift40000', 2, 0, 0),
(300, 'mtngift10000', 2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role_code` varchar(50) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role_code`, `role_name`, `created_by`, `created_date`) VALUES
(1, 'SUPERADMIN', 'Super Admin', 13, '2021-04-01 09:44:42'),
(2, 'ADMIN', 'Admin', 13, '2021-04-01 09:44:50'),
(3, 'STAFFADMIN', 'Staff Admin', 13, '2021-04-01 14:45:25'),
(4, 'USER', 'User', 13, '2021-04-02 11:06:31');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `view` varchar(4) NOT NULL,
  `create_new` varchar(4) NOT NULL,
  `edit` varchar(4) NOT NULL,
  `del` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `role_id`, `module_id`, `view`, `create_new`, `edit`, `del`) VALUES
(1, 1, 18, '1', '1', '1', '1'),
(2, 1, 19, '1', '1', '1', '1'),
(3, 1, 20, '1', '1', '1', '1'),
(4, 1, 21, '1', '1', '1', '1'),
(5, 1, 22, '1', '1', '1', '1'),
(6, 1, 23, '1', '1', '1', '1'),
(7, 1, 24, '1', '1', '1', '1'),
(8, 1, 25, '1', '1', '1', '1'),
(9, 1, 26, '1', '1', '1', '1'),
(10, 1, 27, '1', '1', '1', '1'),
(11, 1, 29, '1', '1', '1', '1'),
(12, 2, 18, '1', '1', '0', '0'),
(13, 2, 19, '1', '1', '1', '0'),
(14, 2, 20, '1', '1', '1', '0'),
(15, 2, 21, '1', '1', '1', '0'),
(16, 2, 22, '1', '1', '1', '1'),
(17, 2, 23, '1', '1', '0', '0'),
(18, 2, 24, '1', '1', '1', '1'),
(19, 2, 25, '1', '1', '0', '0'),
(20, 2, 26, '1', '1', '0', '0'),
(21, 2, 27, '1', '0', '0', '0'),
(22, 2, 29, '1', '1', '1', '0'),
(23, 3, 18, '1', '1', '1', '1'),
(24, 3, 19, '1', '1', '1', '1'),
(25, 3, 20, '1', '1', '1', '1'),
(26, 3, 21, '0', '1', '0', '1'),
(27, 3, 22, '0', '1', '0', '1'),
(28, 3, 23, '1', '0', '0', '1'),
(29, 3, 24, '1', '0', '0', '1'),
(30, 3, 25, '1', '0', '0', '1'),
(31, 3, 26, '0', '0', '0', '1'),
(32, 3, 27, '0', '0', '0', '1'),
(33, 3, 29, '0', '1', '0', '1'),
(34, 4, 18, '0', '0', '0', '0'),
(35, 4, 19, '0', '0', '0', '0'),
(36, 4, 20, '0', '0', '0', '0'),
(37, 4, 21, '0', '0', '0', '0'),
(38, 4, 22, '0', '0', '0', '0'),
(39, 4, 23, '0', '0', '0', '0'),
(40, 4, 24, '0', '0', '0', '0'),
(41, 4, 25, '0', '0', '0', '0'),
(42, 4, 26, '0', '0', '0', '0'),
(43, 4, 27, '0', '0', '0', '0'),
(44, 4, 29, '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(32) NOT NULL,
  `access` int(10) UNSIGNED DEFAULT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `access`, `data`) VALUES
('vfout1jq7ulvsdj802gngbhf05', 1618407331, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_options`
--

CREATE TABLE `site_options` (
  `id` int(11) NOT NULL,
  `option_key` varchar(50) NOT NULL,
  `option_value` longtext NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site_options`
--

INSERT INTO `site_options` (`id`, `option_key`, `option_value`, `updated`) VALUES
(1, 'site_title', '2021 VTU', '2021-03-04 10:08:59'),
(2, 'site_slug', 'We make the world with data', '2021-03-04 10:09:38'),
(5, 'logo', 'uploads/logo/signal.png', '2021-03-04 11:11:17'),
(6, 'min_balance', '50.00', '2021-03-05 12:21:38'),
(7, 'currency', '₦', '2021-03-05 12:25:26'),
(8, 'currency_code', '&#8358;', '2021-03-05 12:26:29'),
(9, 'auto_funding_charge', '80.00', '2021-03-05 12:56:54'),
(10, 'bank_stampduty', '50.00', '2021-03-05 12:58:17'),
(11, 'min_stampduty', '5000.00', '2021-03-05 12:59:33'),
(12, 'bank_details', 'Name: Aderonmu Abisogun<br>\r\nAccount Number: 0013838972<br>\r\nBank: GT Bank', '2021-03-05 16:46:29'),
(13, 'min_fund_request', '100.00', '2021-03-07 12:51:19'),
(14, 'payment_settings', '{\r\n	\"allowMonnifyPayment\": true,\r\n	\"allowPaystackPayment\": true,\r\n	\"allawRavePayment\": false\r\n}', '2021-04-20 16:26:15'),
(15, 'network_codes', '{\r\n	\"MTN NG\": [\"07025\", \"07026\", \"0703\", \"0704\", \"0706\", \"0913\", \"0803\", \"0806\", \"0810\", \"0813\", \"0814\", \"0816\", \"0903\", \"0906\"],\r\n	\"Globacom NG\": [\"0705\", \"0805\", \"0807\", \"0811\", \"0815\", \"0905\"],\r\n	\"Airtel NG\": [\"0701\", \"0708\", \"0802\", \"0808\", \"0812\", \"0901\", \"0902\", \"0904\", \"0907\"],\r\n	\"9mobile\": [\"0809\", \"0817\", \"0818\", \"0909\", \"0908\"]\r\n}', '2021-03-10 11:28:50'),
(16, 'min_airtime_vending', '50', '2021-03-11 12:12:22'),
(17, 'max_airtime_vending', '2500', '2021-03-11 12:12:54'),
(18, 'currency_text', 'NGN', '2021-03-16 10:34:23'),
(19, 'send_webhook_notification', 'true', '2021-03-16 12:15:53'),
(20, 'send_cron_notification', 'true', '2021-03-16 12:17:06'),
(21, 'monnify_settings', '{\r\n	\"baseUrl\": \"https://sandbox.monnify.com\",\r\n	\"apiKey\": \"MK_TEST_GVL2AARSNN\",\r\n	\"secKey\": \"FSRZ78HUQFTR5CJ7KB7RSYL539MRBV8W\",\r\n	\"contractCode\": \"7848618323\",\r\n	\"defaultSplitPercent\": \"20\",\r\n	\"defaultCurrencyCode\": \"NGN\",\r\n	\"charge\": \"60\",\r\n	\"enable\": \"Yes\",\r\n	\"env\": \"sandbox\"\r\n}', '2021-04-19 11:03:18'),
(22, 'sms_sender_id', 'VTUTOPUPBOX', '2021-05-24 09:57:42'),
(23, 'sms_route', 'non_dnd', '2021-05-24 09:57:42'),
(24, 'sms_registration_msg', 'thank you', '2021-05-24 09:56:53'),
(25, 'sms_blocked_registeration', 'kjhkjd', '2021-05-24 09:56:53'),
(26, 'sms_wallet_request_approval', '', '2021-05-24 09:56:53'),
(27, 'sms_order_refund', '', '2021-05-24 09:56:53'),
(28, 'sms_wallet_crediting', '', '2021-05-11 16:28:54'),
(29, 'sms_sender_wallet_share', 'The messagge was sent', '2021-05-24 09:56:53'),
(30, 'sms_receiver_wallet_share', 'The', '2021-05-24 09:56:53'),
(31, 'sms_deduction', '', '2021-05-24 09:56:53'),
(32, 'sms_electricity', '', '2021-05-24 09:56:53'),
(33, 'sms_low_wallet', '', '2021-05-24 09:56:53');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `order_id` varchar(50) DEFAULT NULL,
  `product_plan_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('0','1','2','3','4','5') NOT NULL COMMENT '0 = Declined\r\n1 = Pending\r\n2 = Awaiting Response\r\n3 = Approved\r\n4 = Successful\r\n5 = Refunded',
  `amount` double NOT NULL,
  `amount_charged` double DEFAULT NULL,
  `old_balance` double NOT NULL,
  `balance_after` double NOT NULL,
  `received_by` varchar(100) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `reference`, `order_id`, `product_plan_id`, `date`, `status`, `amount`, `amount_charged`, `old_balance`, `balance_after`, `received_by`, `message`, `user_id`) VALUES
(1, 'ATM_magod', NULL, 1, '2021-03-13 17:28:49', '2', 1, 0, 12125.83, 12125.83, '09036830349', 'Minimum airtime purchase is N10', 13),
(2, 'ATM_zurul', NULL, 1, '2021-03-13 17:38:11', '2', 5, 0, 12125.83, 12125.83, '09036830349', 'Minimum airtime purchase is N10', 13),
(3, 'ATM_lixek', '716914693455', 1, '2021-03-13 18:20:05', '4', 10, 9.9, 12125.83, 12115.93, '09036830349', 'Order successful', 13),
(4, 'ATM_eco8e', NULL, 2, '2021-03-13 20:17:06', '2', 10, 0, 12115.93, 12115.93, '08119900857', 'Airtime topup not completed', 13),
(5, 'ATM_li5ig', NULL, 2, '2021-03-13 20:18:09', '2', 50, 0, 12115.93, 12115.93, '08119900857', 'Airtime topup not completed', 13),
(6, 'ATM_bobo8', NULL, 2, '2021-03-13 20:23:25', '2', 1, 0, 12115.93, 12115.93, '08119900857', 'Airtime topup not completed', 13),
(7, 'ATM_utona', NULL, 2, '2021-03-13 20:24:16', '2', 1, 0, 12115.93, 12115.93, '08119900857', 'Airtime topup not completed', 13),
(8, 'ATM_7emeg', NULL, 2, '2021-03-13 20:25:23', '2', 1, 0, 12115.93, 12115.93, '08119900857', 'Minimum airtime purchase is N10', 13),
(9, 'ATM_3uhoy', NULL, 2, '2021-03-13 20:28:27', '2', 1, 0, 12115.93, 12115.93, '08119900857', 'Minimum airtime purchase is N10', 13),
(10, 'ATM_vakok', '897496945096', 2, '2021-03-13 20:29:55', '4', 10, 9.9, 12115.93, 12106.03, '08119900857', 'GLO N10 to 08119900857 sent successfully', 13),
(11, 'ATM_2osa2', '697167098192', 4, '2021-03-14 15:59:55', '5', 10, 9.9, 5209.7, 5199.8, '09041732405', 'Order Refunded', 12),
(12, 'ATM_esi9i', '324428545340', 2, '2021-03-15 09:00:10', '5', 10, 9.9, 7000.03, 6990.13, '09036830349', 'Order Refunded', 13),
(13, 'ATM_me3ul', '335522955900', 2, '2021-03-15 09:00:31', '5', 10, 9.9, 6990.13, 6980.23, '09036830349', 'Order Refunded', 13),
(14, 'ATM_yi7am', NULL, 4, '2021-03-15 09:09:23', '2', 10, 0, 6980.23, 6980.23, '09036830349', 'Airtime topup not completed', 13),
(15, 'ATM_fi2iv', '338138195472', 2, '2021-03-15 09:10:21', '4', 10, 9.9, 6980.23, 6970.33, '08119900857', 'GLO N10 to 08119900857 sent successfully', 13),
(16, 'ATM_8ikik', '749235648842', 1, '2021-03-15 10:17:21', '5', 10, 9.9, 6970.33, 6960.43, '08119900857', 'Order Refunded', 13),
(17, 'ATM_e7o5u', '527515627423', 1, '2021-03-15 10:40:50', '5', 10, 9.9, 6960.43, 6950.53, '08119900857', 'Order Refunded', 13),
(18, 'ATM_awora', '145997125545', 1, '2021-03-15 11:33:02', '5', 100, 99, 6950.53, 6851.53, '08150736521', 'Order Refunded', 13),
(19, 'ATM_te6ab', '858926129496', 1, '2021-03-15 11:39:05', '4', 20, 19.8, 6851.53, 6831.73, '07030237966', 'MTN N20 to 07030237966 sent successfully', 13),
(20, 'ATM_o5u3u', '287257509947', 1, '2021-03-15 13:22:52', '4', 500, 495, 6831.73, 6336.73, '07030237966', 'MTN N500 to 07030237966 sent successfully', 13),
(21, 'ATM_kivi6', '686073662004', 1, '2021-03-15 13:37:09', '5', 500, 495, 6336.73, 5841.73, '08150736521', 'Order Refunded', 13),
(22, 'ATM_uhowu', '629752917236', 1, '2021-03-15 14:15:02', '5', 20, 19.8, 5841.73, 5821.93, '08150736521', 'Order Refunded', 13),
(23, 'ATM_3akej', '559263237421', 1, '2021-03-15 14:30:53', '5', 10, 9.9, 5821.93, 5812.03, '09784321133', 'Order Refunded', 13),
(24, 'REF_i7oko', '559263237421', 1, '2021-03-15 18:01:56', '5', 10, 9.9, 5812.03, 5821.93, '09784321133', 'Wallet refunded', 13),
(25, 'REF_oqe2i', '629752917236', 1, '2021-03-15 18:04:54', '5', 20, 19.8, 5812.03, 5831.83, '08150736521', 'Order Refunded', 13),
(26, 'REF_ayehe', '629752917236', 1, '2021-03-15 18:08:16', '5', 20, 19.8, 5812.03, 5831.83, '08150736521', 'Wallet refunded', 13),
(27, 'REF_esi6a', '686073662004', 1, '2021-03-15 18:14:28', '5', 500, 495, 5812.03, 6307.03, '08150736521', 'Wallet refunded', 13),
(28, 'REF_goril', '145997125545', 1, '2021-03-15 18:18:09', '5', 100, 99, 6307.03, 6406.03, '08150736521', 'Wallet refunded', 13),
(29, 'REF_amuzi', '527515627423', 1, '2021-03-15 18:19:14', '5', 10, 9.9, 6406.03, 6415.93, '08119900857', 'Wallet refunded', 13),
(30, 'REF_dimik', '749235648842', 1, '2021-03-15 18:19:47', '5', 10, 9.9, 6415.93, 6425.83, '08119900857', 'Wallet refunded', 13),
(31, 'REF_7i0od', '335522955900', 2, '2021-03-15 18:43:17', '5', 10, 9.9, 6425.83, 6435.73, '09036830349', 'Wallet refunded', 13),
(32, 'REF_ejoqo', '324428545340', 2, '2021-03-15 18:55:48', '5', 10, 9.9, 6435.73, 6445.63, '09036830349', 'Wallet refunded', 13),
(33, 'ATM_2axi8', '637935793853', 1, '2021-03-16 10:46:59', '4', 12, 11.88, 6445.63, 6433.75, '09036830349', 'MTN N12 to 09036830349 sent successfully', 13),
(34, 'ATM_apima', NULL, 1, '2021-03-16 10:55:32', '2', 2500, 0, 6433.75, 6433.75, '09036830349', 'Order successful', 13),
(35, 'ATM_ejo8i', '823246875301', 1, '2021-03-16 14:42:22', '5', 12, 11.88, 6433.75, 6421.87, '09346787870', 'Order Refunded', 13),
(36, 'REF_coqu5', '697167098192', 4, '2021-03-16 14:47:11', '5', 10, 9.9, 5199.8, 6431.77, '09041732405', 'Wallet refunded', 12),
(37, 'REF_i7axe', '823246875301', 1, '2021-03-16 14:47:20', '5', 12, 11.88, 6421.87, 6433.75, '09346787870', 'Wallet refunded', 13),
(38, 'ATM_hetak', '530536783930', 4, '2021-03-20 15:55:15', '1', 10, 10, 5209.7, 5199.7, '09036830340', 'Order successful', 12),
(39, 'ATM_ca7i7', '530429853127', 27, '2021-03-24 13:17:13', '4', 100, 100, 6433.75, 6333.75, '09036830349', 'MTN N100 to 09036830349 sent successfully', 13),
(40, 'ATM_uno7i', '386368544112', 27, '2021-03-24 14:12:42', '1', 10, 10, 6333.75, 6323.75, '09036830349', 'Order successful', 13),
(41, 'ATM_wi3uz', '206857396095', 27, '2021-03-25 09:33:07', '4', 100, 100, 6323.75, 6223.75, '09036830349', 'MTN N100 to 09036830349 sent successfully', 13),
(42, 'ATM_ida4i', NULL, 27, '2021-04-03 14:03:28', '2', 100, 0, 5867.75, 5867.75, '09036830349', 'Airtime topup not completed', 13),
(43, 'ATM_a9aku', NULL, 27, '2021-04-03 14:10:14', '2', 100, 0, 5867.75, 5867.75, '09036830349', 'Failed: MTN Airtime is not available at the moment. Please try again later', 13),
(44, 'ATM_jele5', '151253226665', 26, '2021-04-03 14:10:39', '4', 100, 100, 5867.75, 5767.75, '08119900857', 'GLO N100 to 08119900857 sent successfully', 13),
(45, 'ATM_de0u0', '116075709077', 27, '2021-04-13 15:33:01', '5', 10, 10, 5767.75, 5757.75, '08119900857', 'Order Refunded', 13),
(46, 'REF_a9ege', '116075709077', 27, '2021-04-16 14:34:20', '5', 10, 10, 3967.75, 3977.75, '08119900857', 'Wallet refunded', 13),
(47, 'ATM_oduse', '809975958750', 27, '2021-04-16 15:30:58', '4', 10, 10, 4577.75, 4567.75, '09036830349', 'MTN N10 to 09036830349 sent successfully', 13),
(48, 'ETY_4otub', NULL, 111, '2021-05-06 17:24:55', '0', 10, 0, 4567.75, 4567.75, '09031347995', 'Electricity purchase not completed', 13),
(49, 'ETY_oveqi', NULL, 45, '2021-05-06 17:28:12', '0', 10, 0, 4567.75, 4567.75, '09031347995', 'Electricity purchase not completed', 13),
(50, 'ETY_i3evo', NULL, 111, '2021-05-06 19:57:18', '0', 10, 0, 4567.75, 4567.75, '09031347995', 'Electricity purchase not completed', 13),
(51, 'ATM_jo5o6', NULL, 27, '2021-05-24 14:04:09', '0', 50, 0, 5199.7, 5199.7, '09036830349', 'Airtime topup not completed', 12),
(52, 'ATM_efihi', NULL, 177, '2021-05-24 14:41:56', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(53, 'ATM_ubule', NULL, 177, '2021-05-24 14:42:32', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(54, 'ATM_e4eye', NULL, 177, '2021-05-24 14:45:00', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(55, 'ATM_6i4u5', NULL, 177, '2021-05-24 14:46:35', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(56, 'ATM_a8u1o', NULL, 177, '2021-05-24 14:48:06', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(57, 'ATM_adi9u', NULL, 177, '2021-05-24 14:48:54', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(58, 'ATM_co0ow', NULL, 177, '2021-05-24 14:49:29', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(59, 'ATM_ewotu', NULL, 177, '2021-05-24 14:49:48', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(60, 'ATM_ejere', NULL, 177, '2021-05-24 14:50:28', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(61, 'ATM_0u0aw', NULL, 177, '2021-05-24 14:51:17', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(62, 'ATM_uduco', NULL, 177, '2021-05-24 14:51:40', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(63, 'ATM_ace8u', NULL, 177, '2021-05-24 14:51:48', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(64, 'ATM_uzedi', NULL, 177, '2021-05-24 14:52:08', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(65, 'ATM_zaho0', NULL, 177, '2021-05-24 14:52:51', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(66, 'ATM_0o3u8', NULL, 177, '2021-05-24 14:53:01', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(67, 'ATM_panu9', NULL, 177, '2021-05-24 14:53:13', '0', 50, 0, 4557.75, 4557.75, '08119900857', 'Airtime topup not completed', 13),
(68, 'ETY_o0uru', NULL, 195, '2021-05-24 17:57:40', '0', 10, 0, 4507.75, 4507.75, '08119900857', 'Electricity purchase not completed', 13);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_joined` datetime NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `plan_id` int(11) NOT NULL DEFAULT '1',
  `referred_by` int(11) DEFAULT NULL,
  `monnify_reference` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `token` varchar(255) NOT NULL,
  `transaction_pin` varchar(100) DEFAULT NULL,
  `sales_reward` double DEFAULT NULL,
  `sales_amount` double DEFAULT NULL,
  `sales_services` varchar(100) DEFAULT NULL,
  `daily_min_purchase` double DEFAULT NULL,
  `min_wallet_recharge` double DEFAULT NULL,
  `sms_noti` tinyint(1) NOT NULL,
  `sales_period` enum('daily','weekly','monthly') NOT NULL,
  `status` tinyint(1) NOT NULL,
  `disable` tinyint(1) NOT NULL,
  `suspend` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `phone_number`, `email`, `password`, `date_joined`, `role_id`, `plan_id`, `referred_by`, `monnify_reference`, `token`, `transaction_pin`, `sales_reward`, `sales_amount`, `sales_services`, `daily_min_purchase`, `min_wallet_recharge`, `sms_noti`, `sales_period`, `status`, `disable`, `suspend`) VALUES
(12, 'Usman', 'Soliu', '08119900857', 'fresher.dev01@gmail.com', '$2y$12$QHT7.Kpdv1dR1KQYAypSHOOZYO18FDSbMroFySA4w53nlxy2knigu', '2021-03-02 15:18:23', 2, 1, NULL, 'USER_a6e2a9i', '93969e80a96b34aa6d227726438efd6a', '$2y$12$QHT7.Kpdv1dR1KQYAypSHOOZYO18FDSbMroFySA4w53nlxy2knigu', NULL, 8000, NULL, NULL, NULL, 0, 'daily', 1, 0, 0),
(13, 'Global', 'Faaiz', '09036830349', 'soliuomogbolahan01@gmail.com', '$2y$12$QHT7.Kpdv1dR1KQYAypSHOOZYO18FDSbMroFySA4w53nlxy2knigu', '2021-03-02 15:19:09', 1, 2, NULL, 'USER_a6e2a9i', 'b2453079247efe7e2a9d72059bcc4cf5', '$2y$12$QHT7.Kpdv1dR1KQYAypSHOOZYO18FDSbMroFySA4w53nlxy2knigu', 5000, 10000, 'Electricity', NULL, NULL, 0, 'monthly', 1, 0, 0),
(16, 'Global', 'Faaiz', '49093678040', 'tech@globalafaaiz.com', '$2y$12$AEQNARz5tI6cVHQ35sOX7.8u5dpx.ud1.XwaDD9jLveflg5HLJ2fa', '2021-03-04 20:54:33', 1, 1, NULL, 'USER_a6e2a9i', '9f359dcbc98d99c878c3e324685659d3', '$2y$12$QHT7.Kpdv1dR1KQYAypSHOOZYO18FDSbMroFySA4w53nlxy2knigu', NULL, NULL, NULL, NULL, NULL, 0, 'daily', 1, 0, 0),
(17, 'Ajala', 'Jalingo', '08077794701', 'freshest@gmail.com', '$2y$12$kuqLVvw8ONwWA2WTaS2uquKix9ob8UMuEqmFdji7cl.vY2M1mWree', '2021-03-18 12:49:09', 3, 1, NULL, 'USER_a6e2a9i', '522eee646605979ade0a089c736cb445', NULL, 2000, 100000, 'Electricity', 7000, 100, 0, 'weekly', 1, 0, 0),
(20, 'Usman', 'Soliu', '09041732405', 'shittukhay@gmail.com', '$2y$12$cXeWpPD3hFj5VCc5FXlgNOwRobqaNVTszJSGw5YjbJFWkG5d4AyO.', '2021-04-20 10:57:06', 2, 1, NULL, 'USER_a6e2a9i', 'e3190585cd8e07b19b5fbab59a974cd7', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'daily', 1, 0, 0),
(21, 'Ajenifuja', 'Abdulalim', '09093678040', 'techalim@globalfaaiz.com', '$2y$12$aMz2te0TJm8goMI.876BfOvLZtCXPVOYr6qYl2vCaw5qH.zkRjch2', '2021-04-20 10:59:54', 2, 1, NULL, 'USER_uza9a7o', 'ecc62507968879cb50751ae729fee7f2', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'daily', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_monify`
--

CREATE TABLE `users_monify` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_ref` varchar(50) NOT NULL,
  `bank1_name` varchar(50) DEFAULT NULL,
  `bank1_number` varchar(50) DEFAULT NULL,
  `bank1_code` varchar(5) DEFAULT NULL,
  `bank2_name` varchar(50) DEFAULT NULL,
  `bank2_number` varchar(50) DEFAULT NULL,
  `bank2_code` varchar(5) DEFAULT NULL,
  `bank3_name` varchar(50) DEFAULT NULL,
  `bank3_number` varchar(50) DEFAULT NULL,
  `bank3_code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE `user_meta` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `wallet_in`
--

CREATE TABLE `wallet_in` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `old_balance` double NOT NULL,
  `amount` double NOT NULL,
  `balance_after` double NOT NULL,
  `method` enum('auto_fund','manual') DEFAULT NULL,
  `reference` varchar(50) NOT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `description` longtext,
  `type` enum('0','1','2','3','4','5','6') NOT NULL COMMENT '0 = Withdrawal\r\n1 = Fund wallet\r\n2 = Receive Share\r\n3 = Refund Wallet\r\n4 = Purchase\r\n5 = Share Out',
  `status` enum('0','1','2','3','4','5','6') NOT NULL COMMENT '0 = Declined\r\n1 = Pending\r\n2 = Awaiting Response\r\n3 = Approved\r\n4 = Successful\r\n5 = Refunded',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet_in`
--

INSERT INTO `wallet_in` (`id`, `user_id`, `old_balance`, `amount`, `balance_after`, `method`, `reference`, `approved_by`, `description`, `type`, `status`, `date`) VALUES
(56, 12, 0, 4950, 4950, 'manual', 'FWA_a6igi', NULL, '', '1', '3', '2021-03-10 05:36:04'),
(57, 13, 0, 2000, 2000, NULL, 'SWA_9usuz', NULL, '', '2', '3', '2021-03-10 05:37:13'),
(58, 12, 2950, 8420, 2950, 'auto_fund', 'FWA_ka8um', NULL, '', '1', '3', '2021-03-10 08:52:17'),
(59, 12, 11370, 7950, 11370, 'manual', 'FWA_posig', NULL, '', '1', '1', '2021-03-10 09:13:07'),
(60, 12, 11370, 4245, 11370, 'auto_fund', 'FWA_ipi1i', NULL, '', '1', '1', '2021-03-10 09:13:18'),
(61, 13, 2000, 400, 2400, NULL, 'SWA_ocapu', NULL, '', '2', '3', '2021-03-10 09:14:00'),
(62, 13, 2400, 600, 3000, NULL, 'SWA_xaqew', NULL, '', '2', '3', '2021-03-10 09:14:15'),
(63, 13, 3000, 10000, 13000, NULL, 'SWA_vawur', NULL, '', '2', '3', '2021-03-10 09:14:31'),
(64, 13, 12106.03, 100, 12206.03, 'manual', 'FWA_alu1a', NULL, '', '1', '3', '2021-03-14 10:19:12'),
(65, 12, 3.700000000008, 10, 13.700000000008, NULL, 'SWA_e6e8u', NULL, '', '2', '3', '2021-03-14 15:17:36'),
(66, 12, 13.700000000008, 4300, 4313.7, NULL, 'SWA_0iwov', NULL, '', '2', '3', '2021-03-14 15:38:23'),
(67, 12, 4313.7, 896, 5209.7, NULL, 'SWA_elo1e', NULL, '', '2', '3', '2021-03-14 15:40:40'),
(68, 13, 5812.03, 495, 6307.03, '', 'ATM_kivi6', NULL, '', '6', '3', '2021-03-15 18:14:28'),
(69, 13, 6307.03, 99, 6406.03, '', 'ATM_awora', NULL, '', '6', '3', '2021-03-15 18:18:09'),
(70, 13, 6406.03, 9.9, 6415.93, '', 'ATM_e7o5u', NULL, '', '6', '3', '2021-03-15 18:19:14'),
(71, 13, 6415.93, 9.9, 6425.83, '', 'ATM_8ikik', NULL, '', '6', '3', '2021-03-15 18:19:47'),
(72, 13, 6425.83, 9.9, 6435.73, NULL, 'ATM_me3ul', NULL, '', '6', '3', '2021-03-15 18:43:17'),
(73, 13, 6435.73, 9.9, 6445.63, NULL, 'ATM_esi9i', NULL, '', '6', '3', '2021-03-15 18:55:48'),
(74, 12, 5199.8, 9.9, 5209.7, NULL, 'ATM_2osa2', NULL, '', '6', '3', '2021-03-16 14:47:11'),
(75, 13, 6421.87, 11.88, 6433.75, NULL, 'ATM_ejo8i', NULL, '', '6', '3', '2021-03-16 14:47:20'),
(76, 17, 0, 200, 0, 'manual', 'FWA_1ataz', NULL, '', '1', '1', '2021-03-18 12:50:43'),
(77, 12, 5209.7, 200, 5209.7, 'manual', 'FWA_gohe9', NULL, '', '1', '1', '2021-03-19 13:12:11'),
(78, 13, 6223.75, 100, 6223.75, 'manual', 'FWA_ude5o', NULL, '', '1', '1', '2021-04-02 11:19:06'),
(79, 13, 6223.75, 100, 6223.75, 'manual', 'FWA_9aye6', NULL, '', '1', '1', '2021-04-02 11:19:39'),
(80, 13, 6223.75, 100, 6223.75, 'manual', 'FWA_e5i1i', NULL, '', '1', '1', '2021-04-02 11:22:00'),
(81, 13, 6223.75, 100, 6223.75, 'manual', 'FWA_iciza', NULL, '', '1', '1', '2021-04-02 11:22:06'),
(82, 13, 6223.75, 100, 6223.75, 'manual', 'FWA_1idu4', NULL, '', '1', '1', '2021-04-02 11:22:29'),
(83, 13, 6223.75, 100, 6223.75, 'manual', 'FWA_8eka0', NULL, '', '1', '1', '2021-04-02 11:23:15'),
(84, 13, 6223.75, 100, 6223.75, 'manual', 'FWA_2uli5', NULL, '', '1', '1', '2021-04-02 11:24:49'),
(85, 13, 6223.75, 100, 6223.75, 'manual', 'FWA_abagi', NULL, '', '1', '1', '2021-04-02 11:25:02'),
(86, 13, 6223.75, 100, 6323.75, 'manual', 'FWA_oni6i', NULL, '', '1', '3', '2021-04-02 11:28:50'),
(87, 16, 0, 100, 100, 'manual', 'FWA_a4e3u', NULL, '', '1', '3', '2021-04-02 11:29:10'),
(88, 16, 100, 100, 200, 'manual', 'FWA_dafe7', NULL, '', '1', '3', '2021-04-02 11:29:13'),
(89, 16, 200, 68, 268, 'manual', 'FWA_9ozaq', NULL, '', '1', '3', '2021-04-02 11:34:39'),
(90, 16, 268, 80, 348, 'manual', 'FWA_geda3', NULL, '', '1', '3', '2021-04-02 11:35:25'),
(91, 16, 348, 80, 348, 'manual', 'FWA_amo8i', NULL, '', '1', '1', '2021-04-02 11:35:44'),
(92, 16, 348, 80, 428, 'manual', 'FWA_ipita', NULL, '', '1', '3', '2021-04-02 11:37:50'),
(93, 16, 428, 80, 508, 'manual', 'FWA_uwohu', NULL, '', '1', '3', '2021-04-02 11:38:10'),
(94, 16, 508, 80, 588, 'manual', 'FWA_1opuh', NULL, '', '1', '3', '2021-04-02 11:38:45'),
(95, 16, 588, 78, 666, 'manual', 'FWA_kucig', NULL, '', '1', '3', '2021-04-02 11:40:25'),
(96, 16, 666, 78, 666, 'manual', 'FWA_iki9o', NULL, '', '1', '1', '2021-04-02 11:44:12'),
(97, 16, 666, 78, 0, 'manual', 'FWA_ixa8u', NULL, '', '1', '3', '2021-04-02 11:44:30'),
(98, 16, 744, 78, 0, 'manual', 'FWA_vi0uw', NULL, '', '1', '3', '2021-04-02 11:45:21'),
(99, 16, 822, 78, 0, 'manual', 'FWA_onequ', NULL, '', '1', '3', '2021-04-02 11:46:04'),
(100, 16, 900, 78, 0, 'manual', 'FWA_6afit', NULL, '', '1', '3', '2021-04-02 11:46:13'),
(101, 16, 978, 78, 0, 'manual', 'FWA_ewabu', NULL, '', '1', '3', '2021-04-02 11:46:26'),
(102, 16, 1056, 78, 0, 'manual', 'FWA_o2a2a', NULL, '', '1', '3', '2021-04-02 11:46:40'),
(103, 16, 1134, 78, 0, 'manual', 'FWA_te1a5', NULL, '', '1', '3', '2021-04-02 11:46:49'),
(104, 16, 1212, 1000, 0, 'manual', 'FWA_uwuka', NULL, '', '1', '3', '2021-04-02 11:47:06'),
(105, 13, 6323.75, 34, 0, 'manual', 'FWA_eqe9o', NULL, '', '1', '3', '2021-04-02 11:47:44'),
(106, 13, 4357.75, 1600, 0, 'manual', 'FWA_iqita', NULL, '', '1', '3', '2021-04-02 11:52:34'),
(107, 17, 0, 10, 0, 'manual', 'FWA_ihede', NULL, '', '1', '3', '2021-04-02 13:07:16'),
(108, 13, 5757.75, 8000, 0, 'manual', 'FWA_8aguf', NULL, NULL, '1', '3', '2021-04-13 15:47:15'),
(109, 13, 4857.75, 4920, 4857.75, 'auto_fund', 'FWA_fojar', NULL, NULL, '1', '1', '2021-04-14 09:32:45'),
(110, 13, 4857.75, 100, 0, 'manual', 'FWA_e7avi', NULL, NULL, '1', '3', '2021-04-16 11:27:32'),
(111, 13, 4957.75, 100, 0, 'manual', 'FWA_9uye8', NULL, NULL, '1', '3', '2021-04-16 12:03:05'),
(112, 13, 5057.75, 10, 0, 'manual', 'FWA_hi6u4', 13, 'I just love you', '1', '3', '2021-04-16 12:19:27'),
(113, 12, 5199.7, 500, 5699.7, NULL, 'SWA_homu7', NULL, NULL, '2', '4', '2021-04-16 13:14:01'),
(114, 12, 5199.7, 500, 5699.7, NULL, 'SWA_2adi5', NULL, NULL, '2', '4', '2021-04-16 13:14:33'),
(115, 13, 3967.75, 900, 3967.75, 'manual', 'FWA_he9ab', NULL, NULL, '1', '1', '2021-04-16 14:31:53'),
(116, 13, 3967.75, 10, 3977.75, NULL, 'ATM_de0u0', NULL, NULL, '6', '3', '2021-04-16 14:34:20'),
(117, 13, 3977.75, 600, 0, 'manual', 'RWA_ninof', 13, 'You are refunded this ', '3', '3', '2021-04-16 14:44:47'),
(118, 13, 4577.75, 500, 5077.75, 'manual', 'RWA_alise', 13, '', '3', '2', '2021-04-16 14:46:34'),
(119, 13, 4577.75, 100, 4677.75, 'manual', 'RWA_e8e9i', 13, 'Testing', '3', '2', '2021-04-16 15:20:20'),
(120, 21, 0, 700, 0, 'manual', 'FWA_e7ako', NULL, NULL, '1', '1', '2021-04-23 15:43:14'),
(121, 21, 0, 420, 0, 'auto_fund', 'FWA_i0ezu', NULL, NULL, '1', '1', '2021-04-23 16:25:56'),
(122, 12, 5199.7, 10, 5209.7, NULL, 'SWA_oraji', NULL, NULL, '2', '4', '2021-05-24 11:17:36'),
(123, 13, 4557.75, 6920, 4557.75, 'auto_fund', 'FWA_u4asu', NULL, NULL, '1', '1', '2021-05-24 16:44:30'),
(124, 13, 4557.75, 6950, 4557.75, 'manual', 'FWA_4u1ik', NULL, NULL, '1', '1', '2021-05-24 16:44:53'),
(125, 12, 5199.7, 10, 5209.7, NULL, 'SWA_re4u6', NULL, NULL, '2', '4', '2021-05-24 17:00:39'),
(126, 13, 4547.75, 10, 4557.75, NULL, 'SWA_oyu5e', NULL, NULL, '2', '4', '2021-05-24 17:14:04'),
(127, 12, 5199.7, 10, 5209.7, NULL, 'SWA_e3odo', NULL, NULL, '2', '4', '2021-05-24 17:16:06'),
(128, 13, 4527.75, 10, 4537.75, NULL, 'SWA_he0ov', NULL, NULL, '2', '4', '2021-05-24 17:20:01'),
(129, 13, 4517.75, 10, 4527.75, NULL, 'SWA_egagu', NULL, NULL, '2', '4', '2021-05-24 17:48:24'),
(130, 13, 4507.75, 120, 4507.75, 'auto_fund', 'FWA_u1isu', NULL, NULL, '1', '1', '2022-02-17 15:07:50'),
(131, 13, 4507.75, 20, 4507.75, 'auto_fund', 'FWA_o8una', NULL, NULL, '1', '1', '2022-02-17 16:00:24'),
(132, 13, 4507.75, 20, 4507.75, 'auto_fund', 'FWA_ixihe', NULL, NULL, '1', '1', '2022-02-17 16:04:04'),
(133, 12, 5199.7, 90, 5289.7, NULL, 'SWA_giwa9', NULL, NULL, '2', '4', '2022-02-17 16:40:33'),
(134, 12, 5199.7, 5, 5204.7, NULL, 'SWA_buxak', NULL, NULL, '2', '4', '2022-02-17 16:42:07');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_out`
--

CREATE TABLE `wallet_out` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `old_balance` double NOT NULL,
  `amount` double NOT NULL,
  `balance_after` double NOT NULL,
  `reference` varchar(50) NOT NULL,
  `order_id` varchar(50) DEFAULT NULL,
  `description` longtext,
  `type` enum('4','3','5') NOT NULL COMMENT '3=Share Out,4=Purchase\r\n5=Withdrawal',
  `status` enum('0','1','2','3','4','5') NOT NULL COMMENT '0 = Declined\r\n1 = Pending\r\n2 = Awaiting Response\r\n3 = Approved\r\n4 = Successful\r\n5 = Refunded',
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet_out`
--

INSERT INTO `wallet_out` (`id`, `user_id`, `old_balance`, `amount`, `balance_after`, `reference`, `order_id`, `description`, `type`, `status`, `date`) VALUES
(32, 12, 4950, 2000, 2950, 'SWA_9usuz', NULL, '', '3', '3', '2021-03-10 05:37:13'),
(33, 12, 11370, 400, 10970, 'SWA_ocapu', NULL, '', '3', '3', '2021-03-10 09:14:00'),
(34, 12, 10970, 600, 10370, 'SWA_xaqew', NULL, '', '3', '3', '2021-03-10 09:14:15'),
(35, 12, 10370, 10000, 370, 'SWA_vawur', NULL, '', '3', '3', '2021-03-10 09:14:31'),
(38, 13, 13000, 99, 12901, 'ATM_olevi', NULL, '', '4', '3', '2021-03-12 11:41:28'),
(39, 13, 12901, 99, 12802, 'ATM_mo3i3', NULL, '', '4', '3', '2021-03-12 11:41:59'),
(40, 13, 12802, 99, 12703, 'ATM_4a1er', NULL, '', '4', '3', '2021-03-12 12:45:31'),
(41, 13, 12703, 118.8, 12584.2, 'ATM_aruka', NULL, '', '4', '3', '2021-03-12 12:50:46'),
(42, 13, 12584.2, 118.8, 12465.4, 'ATM_du7an', NULL, '', '4', '3', '2021-03-12 12:54:25'),
(43, 13, 12465.4, 118.8, 12346.6, 'ATM_8ohuc', NULL, '', '4', '3', '2021-03-12 12:56:27'),
(44, 13, 12346.6, 99, 12247.6, 'ATM_go6ad', NULL, '', '4', '3', '2021-03-12 13:01:21'),
(45, 13, 12247.6, 121.77, 12125.83, 'ATM_etofa', NULL, '', '4', '3', '2021-03-12 13:04:46'),
(51, 12, 370, 9.9, 360.1, 'ATM_2i2op', NULL, '', '4', '3', '2021-03-12 15:53:37'),
(52, 12, 360.1, 9.9, 350.2, 'ATM_jigeh', NULL, '', '4', '3', '2021-03-12 15:55:03'),
(53, 12, 350.2, 9.9, 340.3, 'ATM_3afox', NULL, '', '4', '3', '2021-03-12 15:57:49'),
(54, 12, 340.3, 9.9, 330.4, 'ATM_lesow', NULL, '', '4', '3', '2021-03-12 16:00:07'),
(55, 12, 330.4, 9.9, 320.5, 'ATM_9alup', NULL, '', '4', '3', '2021-03-12 16:01:13'),
(56, 12, 320.5, 9.9, 310.6, 'ATM_tavex', NULL, '', '4', '3', '2021-03-12 16:01:49'),
(57, 12, 310.6, 9.9, 300.7, 'ATM_uzu6e', NULL, '', '4', '3', '2021-03-12 16:02:50'),
(58, 12, 300.7, 9.9, 290.8, 'ATM_5e3am', NULL, '', '4', '3', '2021-03-12 16:04:57'),
(59, 12, 290.8, 9.9, 280.9, 'ATM_bivup', NULL, '', '4', '3', '2021-03-12 16:48:21'),
(60, 12, 280.9, 9.9, 271, 'ATM_amepa', NULL, '', '4', '3', '2021-03-12 16:48:50'),
(61, 12, 271, 9.9, 261.1, 'ATM_a9uri', NULL, '', '4', '3', '2021-03-12 16:49:42'),
(62, 12, 261.1, 9.9, 251.2, 'ATM_nuvot', NULL, '', '4', '3', '2021-03-12 16:50:21'),
(63, 12, 251.2, 9.9, 241.3, 'ATM_e3aya', NULL, '', '4', '3', '2021-03-12 16:52:22'),
(64, 12, 241.3, 9.9, 231.4, 'ATM_fa5i7', NULL, '', '4', '3', '2021-03-12 16:53:03'),
(65, 12, 231.40000000001, 9.9, 221.50000000001, 'ATM_livaq', NULL, '', '4', '3', '2021-03-12 16:53:55'),
(66, 12, 221.50000000001, 9.9, 211.60000000001, 'ATM_vinif', NULL, '', '4', '3', '2021-03-12 16:54:45'),
(67, 12, 211.60000000001, 9.9, 201.70000000001, 'ATM_vo6ij', NULL, '', '4', '3', '2021-03-12 17:08:27'),
(68, 12, 201.70000000001, 9.9, 191.80000000001, 'ATM_ojuja', NULL, '', '4', '3', '2021-03-12 17:09:28'),
(69, 12, 191.80000000001, 9.9, 181.90000000001, 'ATM_porof', NULL, '', '4', '3', '2021-03-12 17:36:47'),
(70, 12, 181.90000000001, 9.9, 172.00000000001, 'ATM_cehiy', NULL, '', '4', '3', '2021-03-12 17:37:29'),
(71, 12, 172.00000000001, 9.9, 162.10000000001, 'ATM_getey', NULL, '', '4', '3', '2021-03-12 17:40:47'),
(72, 12, 162.10000000001, 9.9, 152.20000000001, 'ATM_li3al', NULL, '', '4', '3', '2021-03-12 17:41:29'),
(73, 12, 152.20000000001, 49.5, 102.70000000001, 'ATM_quxed', NULL, '', '4', '3', '2021-03-12 18:24:33'),
(74, 12, 102.70000000001, 99, 3.700000000008, 'ATM_uxu2a', NULL, '', '4', '3', '2021-03-13 11:56:20'),
(75, 13, 12125.83, 9.9, 12115.93, 'ATM_lixek', '716914693455', '', '4', '3', '2021-03-13 18:20:05'),
(76, 13, 12115.93, 9.9, 12106.03, 'ATM_vakok', '897496945096', '', '4', '3', '2021-03-13 20:29:55'),
(77, 13, 12206.03, 10, 12196.03, 'SWA_e6e8u', NULL, '', '3', '5', '2021-03-14 15:17:36'),
(78, 13, 12196.03, 4300, 7896.03, 'SWA_0iwov', NULL, '', '3', '5', '2021-03-14 15:38:23'),
(79, 13, 7896.03, 896, 7000.03, 'SWA_elo1e', NULL, '', '3', '5', '2021-03-14 15:40:40'),
(80, 12, 5209.7, 9.9, 5199.8, 'ATM_2osa2', '697167098192', '', '4', '3', '2021-03-14 15:59:55'),
(81, 13, 7000.03, 9.9, 6990.13, 'ATM_esi9i', '324428545340', '', '4', '3', '2021-03-15 09:00:10'),
(82, 13, 6990.13, 9.9, 6980.23, 'ATM_me3ul', '335522955900', '', '4', '3', '2021-03-15 09:00:31'),
(83, 13, 6980.23, 9.9, 6970.33, 'ATM_fi2iv', '338138195472', '', '4', '3', '2021-03-15 09:10:21'),
(84, 13, 6970.33, 9.9, 6960.43, 'ATM_8ikik', '749235648842', '', '4', '3', '2021-03-15 10:17:21'),
(85, 13, 6960.43, 9.9, 6950.53, 'ATM_e7o5u', '527515627423', '', '4', '3', '2021-03-15 10:40:50'),
(86, 13, 6950.53, 99, 6851.53, 'ATM_awora', '145997125545', '', '4', '3', '2021-03-15 11:33:02'),
(87, 13, 6851.53, 19.8, 6831.73, 'ATM_te6ab', '858926129496', '', '4', '3', '2021-03-15 11:39:05'),
(88, 13, 6831.73, 495, 6336.73, 'ATM_o5u3u', '287257509947', '', '4', '3', '2021-03-15 13:22:52'),
(89, 13, 6336.73, 495, 5841.73, 'ATM_kivi6', '686073662004', '', '4', '3', '2021-03-15 13:37:09'),
(90, 13, 5841.73, 19.8, 5821.93, 'ATM_uhowu', '629752917236', '', '4', '3', '2021-03-15 14:15:02'),
(91, 13, 5821.93, 9.9, 5812.03, 'ATM_3akej', '559263237421', '', '4', '3', '2021-03-15 14:30:53'),
(92, 13, 6445.63, 11.88, 6433.75, 'ATM_2axi8', '637935793853', '', '4', '3', '2021-03-16 10:46:59'),
(93, 13, 6433.75, 11.88, 6421.87, 'ATM_ejo8i', '823246875301', '', '4', '3', '2021-03-16 14:42:22'),
(94, 12, 5209.7, 10, 5199.7, 'ATM_hetak', '530536783930', '', '4', '3', '2021-03-20 15:55:15'),
(95, 13, 6433.75, 100, 6333.75, 'ATM_ca7i7', '530429853127', '', '4', '3', '2021-03-24 13:17:13'),
(96, 13, 6333.75, 10, 6323.75, 'ATM_uno7i', '386368544112', '', '4', '3', '2021-03-24 14:12:42'),
(97, 13, 6323.75, 100, 6223.75, 'ATM_wi3uz', '206857396095', '', '4', '3', '2021-03-25 09:33:07'),
(98, 13, 6357.75, 2000, 4357.75, 'ADD_9emoc', NULL, '', '3', '5', '2021-04-02 11:51:53'),
(99, 16, 2212, 1600, 612, 'ADD_icafi', NULL, '', '3', '5', '2021-04-02 11:52:56'),
(100, 13, 5957.75, 90, 5867.75, 'ADD_ufo6o', NULL, '', '3', '5', '2021-04-02 12:16:38'),
(101, 13, 5867.75, 100, 5767.75, 'ATM_jele5', '151253226665', '', '4', '3', '2021-04-03 14:10:39'),
(104, 13, 5767.75, 10, 5757.75, 'ATM_de0u0', '116075709077', NULL, '4', '3', '2021-04-13 15:33:01'),
(105, 13, 13757.75, 8900, 4857.75, 'ADD_i0ita', NULL, NULL, '3', '5', '2021-04-13 15:48:04'),
(106, 13, 5067.75, 100, 4967.75, 'ADD_o4afo', NULL, 'I just hate you', '3', '5', '2021-04-16 12:21:54'),
(107, 13, 4967.75, 500, 4467.75, 'SWA_homu7', NULL, NULL, '5', '4', '2021-04-16 13:14:01'),
(108, 13, 4467.75, 500, 3967.75, 'SWA_2adi5', NULL, NULL, '5', '4', '2021-04-16 13:14:33'),
(109, 13, 4577.75, 10, 4567.75, 'ATM_oduse', '809975958750', NULL, '4', '4', '2021-04-16 15:30:58'),
(110, 13, 4567.75, 10, 4557.75, 'SWA_oraji', NULL, NULL, '5', '4', '2021-05-24 11:17:36'),
(111, 13, 4557.75, 10, 4547.75, 'SWA_re4u6', NULL, NULL, '5', '4', '2021-05-24 17:00:39'),
(112, 13, 4547.75, 10, 4537.75, 'SWA_oyu5e', NULL, NULL, '5', '4', '2021-05-24 17:14:04'),
(113, 13, 4537.75, 10, 4527.75, 'SWA_e3odo', NULL, NULL, '5', '4', '2021-05-24 17:16:06'),
(114, 13, 4527.75, 10, 4517.75, 'SWA_he0ov', NULL, NULL, '5', '4', '2021-05-24 17:20:01'),
(115, 13, 4517.75, 10, 4507.75, 'SWA_egagu', NULL, NULL, '5', '4', '2021-05-24 17:48:24'),
(116, 13, 4507.75, 90, 4417.75, 'SWA_giwa9', NULL, NULL, '5', '4', '2022-02-17 16:40:33'),
(117, 13, 4417.75, 5, 4412.75, 'SWA_buxak', NULL, NULL, '5', '4', '2022-02-17 16:42:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `beneficiary`
--
ALTER TABLE `beneficiary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_plan`
--
ALTER TABLE `product_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_options`
--
ALTER TABLE `site_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_monify`
--
ALTER TABLE `users_monify`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_in`
--
ALTER TABLE `wallet_in`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference` (`reference`);

--
-- Indexes for table `wallet_out`
--
ALTER TABLE `wallet_out`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference` (`reference`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beneficiary`
--
ALTER TABLE `beneficiary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `product_plan`
--
ALTER TABLE `product_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `site_options`
--
ALTER TABLE `site_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users_monify`
--
ALTER TABLE `users_monify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallet_in`
--
ALTER TABLE `wallet_in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `wallet_out`
--
ALTER TABLE `wallet_out`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
COMMIT;
