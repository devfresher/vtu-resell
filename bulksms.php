<?php
require_once './components/head.php';
require_once './models/Product.php';
require_once './models/Transaction.php';

$wallet = new Wallet($db);
$product = new Product($db);
$transaction = new Transaction($db);

?>
		<!--begin::Page Vendors Styles(used by this page)-->
		<link href="<?php echo BASE_URL.USER_ROOT?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" style="background-image: url(assets/media/bg/bg-10.jpg)" class="quick-panel-right demo-panel-right offcanvas-right header-fixed subheader-enabled page-loading">
		<!--begin::Main-->
		
		<?php include_once './components/mobileHeader.php'; ?>

		<div class="d-flex flex-column flex-root">

			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">

				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

					<?php include_once './components/toolbar.php';?>

					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

						<?php include_once './components/subToolBar.php'?>

						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">

							<!--begin::Container-->
							<div class="container">

								<!--begin::Wallet-->
								<!--begin::Row-->
								<div class="row">
									<div class="col-xl-3">
										<?php include_once './components/walletBallance.php'?>
									</div>
									<div class="col-xl-9">
                                        <div class="card card-custom card-stretch">
                                            <div class="card-header card-header-tabs-line">
                                                <div class="card-toolbar">
                                                    <ul class="nav nav-tabs nav-bold nav-tabs-line" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" data-toggle="tab" href="#fund_wallet">
                                                                <span class="nav-icon"><i class="fas fa-upload"></i></span>
                                                                <span class="nav-text">Send SMS</span>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#bulksms_history">
                                                                <span class="nav-icon"><i class="fas fa-history"></i></span>
                                                                <span class="nav-text">SMS History</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane fade show active" id="fund_wallet" role="tabpanel" aria-labelledby="kt_tab_pane_1_4">
														<div class="container">
														
															<form class="form" method="POST" id="buy-airtime-form" action="<?php echo BASE_URL?>controllers/product.php">
																<input type="hidden" name="form_url" value="<?php echo BASE_URL.USER_ROOT?>waec">

                                                               
																<div class="form-group" >
																	<label>Sender ID</label>
																	<div class="">
																		<input type="number" id="sender" name="sender" class="form-control" />
																	</div>
																</div>

                                                                <div class="form-group">
                                                                    <label class="col-form-label">Recipient</label>
                                                                    <div class="row">

																	<select name="method" id="recipient_type" class="form-control selectpicker" data-size="4">
                                                                    <option value="type">-- Type Recipient --</option>
                                                                    <option value="upload">-- Upload File --</option>
																		
																		
																						
																	</select>
                                                                    </div>
                                                                </div>
																<div class="form-group" >
																
																	<div class=""  id="file_upload">
																		<input type="file" id="upload" name="upload" class="form-control" />
																	</div>

                                                                    
																	<div class=""   id="type_recipient">
																		<textarea class="form-control" name="bulknumber" placeholder="Separate Numbers with Comma(,)"></textarea>
																	</div>
																</div>

                                                                <div class="form-group" >

																	<label>Secret Key</label>
																	<div class="">
																		<input type="text" id="secret" name="secret" class="form-control" />
																	</div>
																</div>

															

																<input type="submit" name="bulk_sms" class="btn btn-primary mr-2" value="Send SMS" id="bulk_sms" >
															</form>
														</div>
                                                    </div>

                                                    <div class="tab-pane fade" id="bulksms_history" role="tabpanel" aria-labelledby="kt_tab_pane_2_4">
														<div class="card-body">
															<!--begin::Search Form-->
															<div class="mb-7">
																<div class="row align-items-center">
																	<div class="col-lg-9 col-xl-7">
																		<div class="row align-items-center">
																			<div class="col-md-4 my-2 my-md-0">
																				<div class="input-icon">
																					<input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
																					<span>
																						<i class="flaticon2-search-1 text-muted"></i>
																					</span>
																				</div>
																			</div>
																			
                                                                          
																		<a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
																
																		</div>
																	</div>
																
																	<div class="col-lg-2 col-xl-2 mt-5 mt-lg-0">
																		<!--begin::Dropdown-->
																		<div class="dropdown dropdown-inline mr-2">
																			<button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																			<span class="svg-icon svg-icon-md">
																				<!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
																				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																						<rect x="0" y="0" width="24" height="24" />
																						<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
																						<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
																					</g>
																				</svg>
																				<!--end::Svg Icon-->
																			</span>Export</button>
																			<!--begin::Dropdown Menu-->
																			<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
																				<!--begin::Navigation-->
																				<ul class="navi flex-column navi-hover py-2">
																					<li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Choose an option:</li>
																					<li class="navi-item">
																						<a href="#" class="navi-link">
																							<span class="navi-icon">
																								<i class="la la-print"></i>
																							</span>
																							<span class="navi-text">Print</span>
																						</a>
																					</li>
																					<li class="navi-item">
																						<a href="#" class="navi-link">
																							<span class="navi-icon">
																								<i class="la la-file-pdf-o"></i>
																							</span>
																							<span class="navi-text">PDF</span>
																						</a>
																					</li>
																				</ul>
																				<!--end::Navigation-->
																			</div>
																			<!--end::Dropdown Menu-->
																		</div>
																		<!--end::Dropdown-->
																	</div>
																</div>
															</div>
															<!--end: Search Form-->

															<!--begin: Datatable-->
															<table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
																<thead>
																	<tr>
																		<th title="Field #1" class="custom-th">S/N</th>
																		<th title="Field #2" class="custom-th">Sender ID</th>
																		<th title="Field #2" class="custom-th">Recipient</th>
																		<th title="Field #2" class="custom-th">Message</th>
																		<th title="Field #3" class="custom-th">Old Balance</th>
																		<th title="Field #2" class="custom-th">Amount Charged</th>
																		<th title="Field #4" class="custom-th">New Balance</th>
																		<th title="Field #4" class="custom-th">Status</th>
																		<th title="Field #5" class="custom-th">Date</th>
																	</tr>
																</thead>
																<tbody>
																	<?php if($purchaseHistory !== false){
																	 	foreach ($purchaseHistory as $history) {?>
																			<tr>
																				<td><?php echo $utility->niceDateFormat($history['date']).'<br><br><strong>'.$history['reference']?></strong></td>
																				<td>
																					<?php echo $history['product_name'].' '.$history['amount'].' - '.'<strong>'.$history['received_by'].'</strong>'?>
																				</td>
																				<td>
																					<label class="label font-weight-bold label-lg label-light-info label-inline">Old: <?php echo $appInfo->currency_code.number_format($history['old_balance'],2)?></label><br><br>
																					<strong>Amount Charged: </strong> <?php echo $appInfo->currency_code.number_format($history['amount_charged'], 2)?><br><br>
																					<label class="label font-weight-bold label-lg label-light-success label-inline">New: <?php echo $appInfo->currency_code.number_format($history['balance_after'],2)?></label>
																				</td>
																				<td><?php echo $history['status']?></td>
																				<td>
																					<?php echo $history['message']?>
																					<?php echo ($history['status'] == 1) ? '<a href="'. BASE_URL.'webhook?requery&id='.$history['order_id'].'" class="btn btn-danger btn-sm requeryBtn" data-orderId="'.$history['order_id'].'">Requery</a>':''?>
																					<?php echo ($history['status'] != 1) ? '<a href="'. BASE_URL.'receipt?refId='.$history['reference'].'" data-orderId="'.$history['order_id'].'"><i class="text-success fas fa-print"></i></a>':''?>
																				</td>
																			</tr>
																		<?php } ?>
																	<?php } ?>
																</tbody>
															</table>
															<!--end: Datatable-->
														</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								</div>
								<!--end::Wallet-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!--end::Content-->
					
					<?php include_once './components/footer.php';?>

				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->

		<?php include_once './components/quickUser.php';?>

		<?php include_once './components/scrollTop.php';?>
		<?php include_once './components/js.php';?>
		
		<!--begin::Page Scripts(used by this page)-->
		<script src="<?php echo BASE_URL?>assets/js/pages/crud/ktdatatable/base/txn-table.js"></script>
		<script src="<?php echo BASE_URL?>assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
		<?php include_once './components/message.php'?>

		<script>
            
                    $('#file_upload').hide();
                   // $('#type_recipient').hide();

            $('#recipient_type').on('change', function () {
                var check = $('#recipient_type').val();
                
                if(check == 'upload'){

                    $('#file_upload').show();
                    $('#type_recipient').hide();

                }else{

                    $('#file_upload').hide();
                    $('#type_recipient').show();
                }
               
            })


        </script>
		<!--end::Page Scripts-->
	</body>
	<!--end::Body-->
</html>